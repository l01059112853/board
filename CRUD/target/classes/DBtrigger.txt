/*
*	board Trigger
*	게시글 삭제 시 그 게시글에 존재했던 댓글, 파일들 모두 삭제
*/

DELIMITER $$
CREATE TRIGGER boardDeleteTrigger
AFTER DELETE ON board
FOR EACH ROW
BEGIN
    DELETE FROM comment WHERE root = OLD.no;
    DELETE FROM boardUploadFile WHERE no = OLD.no;
    DELETE FROM boardThumbnail WHERE no = OLD.no;
END$$
DELIMITER ;


/*
* board Trigger
* 게시글 추가 시 썸네일 테이블에 데이터 추가
*/

DELIMITER $$
CREATE TRIGGER boardInsertTrigger
AFTER INSERT ON board
FOR EACH ROW
BEGIN
	INSERT INTO boardThumbnail(no, path)
	VALUES(NEW.no, "resources/IMG/noImage.png");
	
	INSERT INTO boardUploadFile(no)
	VALUES(NEW.no);
END$$
DELIMITER ;


/*
* signUp Trigger
* 회원가입 시 memberProfile에 데이터 추가
*/

DELIMITER $$
CREATE TRIGGER signUpTrigger
AFTER INSERT ON member
FOR EACH ROW
BEGIN
	INSERT INTO memberProfile(id, path)
	VALUES(NEW.id, "");
END$$
DELIMITER ;