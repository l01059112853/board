<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>${userID}'s Page</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="">
		<meta name="author" content="">

		<link rel="stylesheet" href="resources/CSS/mypage.css?v=3" />
	</head>
	<body class="is-preload">
		
		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="#" class="image avatar"><img src="${path}" alt="" id="myProfileImage"></a>
					<h1><strong>${userID}</strong></h1>
					<h6 style="color:rgba(255,255,255,.5);">${userID}'s activity details.</h6>
					
					<c:if test="${isLogin == true}">
						<c:if test="${member.id == userID}">
							<form id="uploadForm" action="uploadImage" method="post" enctype="multipart/form-data">
								<label for="uploadImage" id="uploadLabel"><h6>› 프로필사진 변경</h6></label>
								<input type="file" id="uploadImage" name="uploadImage" accept=".gif, .jpg, .png">
							</form>
						</c:if>
					</c:if>
					
					back to the <a href="#" onclick="location.href='<%=request.getContextPath()%>/'">Main</a></h1>
				</div>
			</header>
		<!-- Main -->
			<div id="main">
				<!-- One -->
					<section id="one">
						<h2>최근 작성한 게시글</h2>
						
						<c:choose>
							<c:when test="${boardStatus == false}">
						<div class="row">
							<h4>작성한 게시글이 존재하지 않습니다.</h4>
						</div>
							</c:when>
							<c:otherwise>
						<div class="row">
								<c:forEach var="board" items="${boardList}" varStatus="idx">
							<article class="col-6 col-12-xsmall work-item">
								<a href="#" onclick="location.href='<%=request.getContextPath()%>/detail?no=${board.no}'" class="image fit thumb"><img src="${boardThumbnailList[idx.index].path}" style="object-fit:cover;"alt="" ></a>
								<h3>${board.subject}</h3>
								<p>${board.date}</p>
							</article>
								</c:forEach>
						</div>
							</c:otherwise>
						</c:choose>
					</section>

					<section id="two">
						<h2>최근 댓글을 단 게시글</h2>
						
						<c:choose>
							<c:when test="${commentStatus == false}">
						<div class="row">
							<h4>작성한 댓글이 존재하지 않습니다.</h4>
						</div>
							</c:when>
							<c:otherwise>
						<div class="row">
							<c:forEach var="cmtBoard" items="${boardListByComment}" varStatus="idx">
							<c:if test="${cmtBoard != null}">
							<article class="col-6 col-12-xsmall work-item">
								<a href="#" onclick="location.href='<%=request.getContextPath()%>/detail?no=${cmtBoard.no}'" class="image fit thumb"><img src="${boardThumbnailListByComment[idx.index].path}" style="object-fit:cover;" alt=""></a>
								<h3>${cmtBoard.subject}</h3>
								<p>${commentListByWriter[idx.index].body}</p>
							</article>
							</c:if>
							</c:forEach>
						</div>
							</c:otherwise>
						</c:choose>
					</section>
			</div>

		<!-- Scripts -->
			<script src="resources/JS/jquery.min.js"></script>
			<script src="resources/JS/jquery.poptrox.min.js"></script>
			<script src="resources/JS/browser.min.js"></script>
			<script src="resources/JS/breakpoints.min.js"></script>
			<script src="resources/JS/util.js"></script>
			<script src="resources/JS/mypage.js"></script>

	</body>
</html>