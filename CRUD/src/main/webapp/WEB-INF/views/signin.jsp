<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
	    <title>BoardList</title>
	    <meta charset="utf8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1" />
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
	    <link rel="stylesheet" href="resources/CSS/signin.css?v=3"/>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	    <script src="resources/JS/signin.js"></script>
	</head>
	
	<body>
	<form method="post" action="<%=request.getContextPath()%>/signin.do">
		<div class="Container">
			<div class = "wrap" style="display:flex;flex-direction:column;justify-content:center;align-items:center;height:100%;padding-bottom:200px;">
				<div class="form-group" style="display: flex; flex-direction: column; align-items: center; padding-top: 30px; color: #fff;">
					<h1>Sign In</h1>
				</div>

				<div class="form-group">
					<label for="id" style="color:#fff;">ID:</label>
					<input type="text" class="form-control" name="id" id="id">
				</div>

				<div class="form-group">
					<label for="password" style="color:#fff;">Password:</label>
					<input type="password" class="form-control" name="pw" id="pw">
				</div>

				<div class="form-group"
					style="display: flex; justify-content: flex-end; padding-top: 50px;">
					<button type="submit" class="btn btn-primary" id="submitBtn" disabled="true">login</button>
					&nbsp;
					<button type="button" class="btn btn-warning" onclick="location.href='<%=request.getContextPath()%>/signup'">sign up</button>
					&nbsp;
					<button type="button" class="btn btn-danger" onclick="location.href='<%=request.getContextPath()%>/'">cancel</button>
				</div>
			</div>
		</div>
	</form>
	</body>
</html>
