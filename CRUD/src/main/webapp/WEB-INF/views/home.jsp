<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>realize CRUD board</title>
	
	<!-- Font Awesome Icons -->
	<link href="resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
	<link href="resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<!-- Theme CSS - Includes Bootstrap -->
	<link href="resources/CSS/creative.min.css" rel="stylesheet">
	
</head>
<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
		<div class="container">
			<c:choose>
				<c:when test="${isLogin == true}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default" onclick="location.href='<%=request.getContextPath()%>/mypage/${member.id}'">${member.id}</a>
				</c:when>
				<c:when test="${isLogin == false}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default">not login status.</a>
				</c:when>
			</c:choose>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<c:choose>
						<c:when test="${isLogin == false}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signin">sign in</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signup">sign up</a>
						</li>
						</c:when>
						<c:when test="${isLogin == true}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/logout">logout</a>
						</li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Masthead -->
	<header class="masthead">
		<div class="container h-100">
			<div class="row h-100 align-items-center justify-content-center text-center">
				<div class="col-lg-10 align-self-end">
					<h1 class="text-uppercase text-white font-weight-bold">realize CRUD board in Spring framework</h1>
					<hr class="divider my-4">
				</div>
				<div class="col-lg-8 align-self-baseline">
					<p class="text-white-75 font-weight-light mb-5">create read update delete board in spring framework. OS is Ubuntu 18.04. DB use mariaDB. All is environment that AWS EC2. project releases with FileZilla.</p>
					<a class="btn btn-primary btn-xl js-scroll-trigger" href="<%=request.getContextPath()%>/list">Board List</a>
				</div>
			</div>
		</div>
	</header>
	
  <!-- Bootstrap core JavaScript -->
  <script src="resources/vendor/jquery/jquery.min.js"></script>
  <script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="resources/JS/creative.min.js"></script>
  
</body>
</html>
