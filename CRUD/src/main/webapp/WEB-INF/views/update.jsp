<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>${board.writer} : ${board.subject} modify</title>
	<meta charset="utf8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="stylesheet" href="resources/CSS/list.css?v=3" />

	<!-- Font Awesome Icons -->
	<link href="resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
	<link href="resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<!-- Theme CSS - Includes Bootstrap -->
	<link href="resources/CSS/creative.min.css?v=3" rel="stylesheet">
</head>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
		<div class="container">
			<c:choose>
				<c:when test="${isLogin == true}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default" onclick="location.href='<%=request.getContextPath()%>/mypage/${member.id}'">${member.id}</a>
				</c:when>
				<c:when test="${isLogin == false}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default">not login status.</a>
				</c:when>
			</c:choose>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<c:choose>
						<c:when test="${isLogin == false}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signin">sign in</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signup">sign up</a>
						</li>
						</c:when>
						<c:when test="${isLogin == true}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/logout">logout</a>
						</li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</div>
	</nav>
	<form method="post" action="update.do" enctype="multipart/form-data">
		<div class="Container">

			<input type="hidden" name="no" id="no" value="${board.no}"/>

			<div class="form-group"
				style="display: flex; flex-direction: column; align-items: center; padding-top: 30px;">
				<h2>${board.subject} Modify</h2>
				<p>Please fill in all forms!</p>
			</div>

			<div class="form-group">
				<label for="user">Writer:</label> <input type="text" class="form-control" id="writer" name="writer" style="background-color: #343a40;color:#fff;" value="${board.writer}" readonly="true">
			</div>

			<div class="form-group">
				<label for="subject">Subject:</label> <input type="text"
					class="form-control" id="subject" name="subject" style="background-color: #343a40;color:#fff;" value="${board.subject}">
			</div>

			<div class="form-group">
				<label for="body">Body:</label>
				<textarea class="form-control" rows="5" id="body" name="body" style="background-color: #343a40;color:#fff;">${board.body}</textarea>
			</div>
			
			<div class="form-group">
				<label for="uploadThumbnailLbl">Thumbnail:</label>
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="boardThumbnail" name="thumbnail" style="cursor:pointer;" accept=".gif, .jpg, .png">
					<label class="custom-file-label" id="boardThumbnailLabel" name="boardThumbnailLabel" for="boardThumbnail" style="background-color:#343a40;color:#fff;"></label>
					<input type="hidden" id="thumbnailHiddenPath" name="thumbnailHiddenPath" />
				</div>
			</div>
						
			<div class="form-group">
				<label for="uploadFileLbl">File:</label>
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="boardFile" name="file" style="cursor:pointer;">
					<label class="custom-file-label" id="boardFileLabel" name="boardFileLabel" for="boardFile" style="background-color:#343a40;color:#fff;"></label>
					<input type="hidden" id="fileHiddenPath" name="fileHiddenPath" />
				</div>
			</div>

			<div class="form-group" style="display: flex; justify-content: flex-end;">
				<input type="submit" class="btn btn-primary" id="modifyBtn" value="modify" disabled="true"/>&nbsp;
				<input type="button" class="btn btn-warning" value="cancel" onclick="location.href='<%=request.getContextPath()%>/detail?no=${board.no}'"/>
			</div>
		</div>
	</form>
	
  <!-- Bootstrap core JavaScript -->
  <script src="resources/vendor/jquery/jquery.min.js"></script>
  <script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="resources/JS/creative.min.js"></script>
  
  <script src="resources/JS/update.js"></script>
</body>
</html>
