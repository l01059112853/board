<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>BoardList</title>
	<meta charset="utf8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="stylesheet" href="resources/CSS/list.css" />

	<!-- Font Awesome Icons -->
	<link href="resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
	<link href="resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<!-- Theme CSS - Includes Bootstrap -->
	<link href="resources/CSS/creative.min.css" rel="stylesheet">
</head>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
		<div class="container">
			<c:choose>
				<c:when test="${isLogin == true}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default" onclick="location.href='<%=request.getContextPath()%>/mypage/${member.id}'">${member.id}</a>
				</c:when>
				<c:when test="${isLogin == false}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default">not login status.</a>
				</c:when>
			</c:choose>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<c:choose>
						<c:when test="${isLogin == false}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signin">sign in</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signup">sign up</a>
						</li>
						</c:when>
						<c:when test="${isLogin == true}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/logout">logout</a>
						</li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</div>
	</nav>

	<form>
		<div class="Container">
			<div class="form-group"
				style="display: flex; flex-direction: column; align-items: center; padding-top: 80px;">
				<h2 style="font-weight: 600;">CRUD Board</h2>
				<p>
					There are currently <span
						style="color: firebrick; font-weight: 700;">${boardCount}</span> postings.
				</p>
			</div>

			<ul class="countdown">
				<li><span class="hours" id="hours">00</span>
					<p class="hours_ref">hours</p></li>
				<li class="seperator">:</li>
				<li><span class="minutes" id="minutes">00</span>
					<p class="minutes_ref">minutes</p></li>
				<li class="seperator">:</li>
				<li><span class="seconds" id="seconds">00</span>
					<p class="seconds_ref">seconds</p></li>
			</ul>

			<table class="table table-light table-hover">
				<thead class="thead-light">
					<tr>
						<th>no</th>
						<th>writer</th>
						<th>subject</th>
						<th>date</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="board" items="${board}" varStatus="idx">
						<tr onclick="location.href='<%=request.getContextPath()%>/detail?no=${board.no}'" style="cursor:hand">
							<td>${board.no}</td>
							<td>${board.writer}</td>
							<c:choose>
 								<c:when test="${boardCmt[idx.index] <= 0}">
							<td>${board.subject} </td>
								</c:when>
								<c:otherwise>
							<td>${board.subject} <span class="badge badge-danger">${boardCmt[idx.index]}</span> </td>
								</c:otherwise>
							</c:choose>
							<td>${board.date}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="form-group" style="display: flex; justify-content: flex-end;">
				<input type="button" class="btn btn-warning" value="write" onclick="location.href='<%=request.getContextPath()%>/write'"/>&nbsp;
				<input type="button" class="btn btn-primary" value="home" onclick="location.href='<%=request.getContextPath()%>/'"/>&nbsp;
				<c:choose>
					<c:when test="${search eq '' || empty search}">
						<input type="button" class="btn btn-dark" value="grid로 보기" onclick="location.href='<%=request.getContextPath()%>/grid'"/>
					</c:when>
					<c:otherwise>
						<input type="button" class="btn btn-dark" value="grid로 보기" onclick="location.href='<%=request.getContextPath()%>/grid?search=${search}'" />
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="form-group" style="display:flex;justify-content:center;align-items:center;padding-top:50px;">
				<div class="input-group mb-3" style="width:400px;">
					<input type="text" class="form-control" placeholder="Search" name="searchInput" id="searchInput">
					<div class="input-group-append">
						<button class="btn btn-success" type="button" onclick="search()">Go</button>
					</div>
				</div>
			</div>

			<c:set var="totalPage" value="${map['totalPage']}" />
			<c:set var="screenPage" value="${map['screenPage']}" />
			<c:set var="screenBoard" value="${map['screenBoard']}" />
			<c:set var="currentPage" value="${map['currentPage']}" />
			<c:set var="currentHead" value="${map['currentHead']}" />
			<c:set var="currentTail" value="${map['currentTail']}" />
			
			<!-- totalBoard, totalPage, screenPage, screenBoard, currentPage, currentHead, currentTail -->
			<div class="footer" style="display: flex; justify-content: center; padding-top: 150px;">
				<ul class="pagination">
				
				<c:choose>
					<c:when test="${search == null}">
						<c:if test="${currentPage-screenPage > 0}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentHead-1}"/>prev</a></li>
						</c:if>
						<c:forEach var="pageItem" begin="${currentHead}" end="${currentTail}">
							<c:choose>
								<c:when test="${pageItem == currentPage}">
							<li class="page-item active"><a class="page-link" href="javascript:void(0);">${pageItem}</a></li>
								</c:when>
								<c:otherwise>
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${pageItem}">${pageItem}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:if test="${totalPage > (currentHead+screenPage)-1}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentTail+1}">next</a></li>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${currentPage-screenPage > 0}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentHead-1}&search=${search}"/>prev</a></li>
						</c:if>
						<c:forEach var="pageItem" begin="${currentHead}" end="${currentTail}">
							<c:choose>
								<c:when test="${pageItem == currentPage}">
							<li class="page-item active"><a class="page-link" href="javascript:void(0);">${pageItem}</a></li>
								</c:when>
								<c:otherwise>
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${pageItem}&search=${search}">${pageItem}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:if test="${totalPage > (currentHead+screenPage)-1}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentTail+1}&search=${search}">next</a></li>
						</c:if>					
					</c:otherwise>
				</c:choose>
				</ul>
			</div>
			
		</div>
	</form>
	
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/JS/creative.min.js"></script>
  
	<script src="resources/JS/listTimer.js"></script>
  	<script src="resources/JS/list.js"></script>
</body>
</html>
