<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>BoardList</title>
    <meta charset="utf8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/CSS/signup.css?v=3"/>
</head>
<body>
	<form method="post" action="<%=request.getContextPath()%>/signup.do">
		<div class="Container">

			<div class = "wrap" style="display:flex;flex-direction:column;justify-content:center;align-items:center;height:100%;padding-bottom:200px;">
				<div class="form-group"
					style="display: flex; flex-direction: column; align-items: center; padding-top: 30px; color: #fff;">
					<h1>Sign Up</h1>
				</div>

				<div class="form-group">
					<label for="id" style="color:#fff;">ID:</label>
					<input type="text" name="id" id="id" class="form-control" placeholder="최소 4글자"/> <small id="idError" class="errorMsg" style="color: #fff"></small>
				</div>

				<div class="form-group">
					<label for="password" style="color:#fff;">Password:</label>
					<input type="password" class="form-control" name="pw" id="pw">
				</div>

				<div class="form-group">
					<label for="name" style="color:#fff;">Name:</label>
					<input type="text" class="form-control" name="name" id="name">
				</div>

				<div class="form-group" style="display: flex; justify-content: flex-end; padding-top: 50px;">
					<button type="submit" class="btn btn-primary" id="submitBtn" disabled="true">sign up</button>&nbsp;
					<button type="button" class="btn btn-warning" onclick="location.href='<%=request.getContextPath()%>/signin'">sign in</button>&nbsp;
					<button type="button" class="btn btn-danger" onclick="location.href='<%=request.getContextPath()%>/'">cancel</button>
				</div>
			</div>
		</div>
	</form>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="resources/JS/signup.js"></script>	
</body>
</html>
