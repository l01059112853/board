<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>BoardList</title>
	<meta charset="utf8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="stylesheet" href="resources/CSS/grid.css?v=3" />

	<!-- Font Awesome Icons -->
	<link href="resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
	<link href="resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<!-- Theme CSS - Includes Bootstrap -->
	<link href="resources/CSS/creative.min.css?v=3" rel="stylesheet">
</head>

<body id="page-top">

	<!-- remote -->
	<a href="#page-top" id="remoteBtn">
		<div class="remote">
			<span>▲</span>
		</div>
	</a>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
		<div class="container">
			<c:choose>
				<c:when test="${isLogin == true}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default" onclick="location.href='<%=request.getContextPath()%>/mypage/${member.id}'">${member.id}</a>
				</c:when>
				<c:when test="${isLogin == false}">
				<a class="navbar-brand js-scroll-trigger" style="cursor:default">not login status.</a>
				</c:when>
			</c:choose>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<c:choose>
						<c:when test="${isLogin == false}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signin">sign in</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/signup">sign up</a>
						</li>
						</c:when>
						<c:when test="${isLogin == true}">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger" href="<%=request.getContextPath()%>/logout">logout</a>
						</li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</div>
	</nav>

	<form>
		<div class="Container">
		
			<!-- There are currently N postings. -->
			<div class="form-group" style="display: flex; flex-direction: column; align-items: center; padding-top: 80px;">
				<h1 class="display-3" style="font-family: Playfair Display">CRUD Board</h1>
			</div>
			
			<!-- search Area -->
			<div class="form-group" style="display:flex;justify-content:center;align-items:center;padding-bottom:20px;">
				<div class="input-group mb-3" style="width:600px;">
					<input type="text" class="form-control" placeholder="Search" name="searchInput" id="searchInput">
					<div class="input-group-append">
						<button class="btn btn-success" type="button" style="border-top-right-radius: .25rem;border-bottom-right-radius: .25rem;" onclick="search()">Go</button>
					</div>
					&nbsp;<input type="button" class="btn btn-warning" value="write" onclick="location.href='<%=request.getContextPath()%>/write'"/>
					&nbsp;<input type="button" class="btn btn-primary" value="home" onclick="location.href='<%=request.getContextPath()%>/'"/>
				<c:choose>
					<c:when test="${search eq '' || empty search}">
						&nbsp;<input type="button" class="btn btn-dark" value="list로 보기" onclick="location.href='<%=request.getContextPath()%>/list'"/>
					</c:when>
					<c:otherwise>
						&nbsp;<input type="button" class="btn btn-dark" value="list로 보기" onclick="location.href='<%=request.getContextPath()%>/list?search=${search}'" />
					</c:otherwise>
				</c:choose>
				</div>
			</div>
			
			<!-- main area -->
			<div class="container-fluid">
			<c:forEach var = "board" items="${board}" varStatus="idx">
			<c:if test="${idx.index % 3 == 0}">
				<div class="board_lists rowMedia">
			</c:if>
					<div class="col-xs-3 colMedia">
						<div class="boardThumbnail">
							<a href="<%=request.getContextPath()%>/detail?no=${board.no}">
							<c:choose>
								<c:when test="${idx.first}">
								<img src="${boardThumbnail[idx.index]}" id="firstImage" alt="" class="thumbnailImage">
								</c:when>
								<c:otherwise>
								<img src="${boardThumbnail[idx.index]}" alt="" class="thumbnailImage">
								</c:otherwise>
							</c:choose>
								<div class="properties">
									<div class="property">
									💬${boardCmt[idx.index]}<c:if test="${boardUploadFile[idx.index]}">&nbsp;&nbsp;&nbsp;🔗</c:if>
									</div>
								</div>
							</a>
						</div>
					</div>
				<c:if test="${idx.index % 3 == 2}">
				</div>
				</c:if>
			</c:forEach>
			</div>
			
			<input type = "hidden" id="gridScreenCount" name="gridScreenCount" value="${gridScreenCount}" />
			<input type = "hidden" id="gscSum" name="gscSum" value="${gridScreenCount}" />
			
			<div class="form-group bottomAlign">
				<input type="button" id="seeMore" class="btn btn-light" onClick="getBoard(${search})" value="see More"/>
			</div>

<%-- 			<c:set var="totalPage" value="${map['totalPage']}" />
			<c:set var="screenPage" value="${map['screenPage']}" />
			<c:set var="screenBoard" value="${map['screenBoard']}" />
			<c:set var="currentPage" value="${map['currentPage']}" />
			<c:set var="currentHead" value="${map['currentHead']}" />
			<c:set var="currentTail" value="${map['currentTail']}" />
			
			<!-- totalBoard, totalPage, screenPage, screenBoard, currentPage, currentHead, currentTail -->
			<div class="footer" style="display: flex; justify-content: center; padding-top: 150px;">
				<ul class="pagination">
				
				<c:choose>
					<c:when test="${search == null}">
						<c:if test="${currentPage-screenPage > 0}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentHead-1}"/>prev</a></li>
						</c:if>
						<c:forEach var="pageItem" begin="${currentHead}" end="${currentTail}">
							<c:choose>
								<c:when test="${pageItem == currentPage}">
							<li class="page-item active"><a class="page-link" href="javascript:void(0);">${pageItem}</a></li>
								</c:when>
								<c:otherwise>
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${pageItem}">${pageItem}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:if test="${totalPage > (currentHead+screenPage)-1}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentTail+1}">next</a></li>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${currentPage-screenPage > 0}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentHead-1}&search=${search}"/>prev</a></li>
						</c:if>
						<c:forEach var="pageItem" begin="${currentHead}" end="${currentTail}">
							<c:choose>
								<c:when test="${pageItem == currentPage}">
							<li class="page-item active"><a class="page-link" href="javascript:void(0);">${pageItem}</a></li>
								</c:when>
								<c:otherwise>
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${pageItem}&search=${search}">${pageItem}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:if test="${totalPage > (currentHead+screenPage)-1}">
							<li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/list?page=${currentTail+1}&search=${search}">next</a></li>
						</c:if>					
					</c:otherwise>
				</c:choose>
				</ul>
			</div> --%>
			
		</div>
	</form>
	
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/JS/creative.min.js"></script>
  
  	<script src="resources/JS/grid.js"></script>
</body>
</html>
