<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>${board.writer} : ${board.subject}</title>
	<meta charset="utf8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
    
    <link rel="stylesheet" href="resources/CSS/detail.css?v=3">
    
	<!-- Font Awesome Icons -->
	<link href="resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
	<link href="resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<!-- Theme CSS - Includes Bootstrap -->
	<link href="resources/CSS/creative.min.css?v=3" rel="stylesheet">
</head>
<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3"
		id="mainNav">
		<div class="container">
			<c:choose>
				<c:when test="${isLogin == true}">
					<a class="navbar-brand js-scroll-trigger" style="cursor:default" onclick="location.href='<%=request.getContextPath()%>/mypage/${member.id}'">${member.id}</a>
				</c:when>
				<c:when test="${isLogin == false}">
					<a class="navbar-brand js-scroll-trigger" style="cursor: default">not
						login status.</a>
				</c:when>
			</c:choose>
			<button class="navbar-toggler navbar-toggler-right" type="button"
				data-toggle="collapse" data-target="#navbarResponsive"
				aria-controls="navbarResponsive" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0">
					<c:choose>
						<c:when test="${isLogin == false}">
							<li class="nav-item"><a class="nav-link js-scroll-trigger"
								href="<%=request.getContextPath()%>/signin">sign in</a></li>
							<li class="nav-item"><a class="nav-link js-scroll-trigger"
								href="<%=request.getContextPath()%>/signup">sign up</a></li>
						</c:when>
						<c:when test="${isLogin == true}">
							<li class="nav-item"><a class="nav-link js-scroll-trigger"
								href="<%=request.getContextPath()%>/logout">logout</a></li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</div>
	</nav>
	<form id="detailForm" method="post" action="update">
		<div class="Container" style="padding-top:100px;">
			<p><span class="badge badge-dark">${board.date}</span></p>
		<c:if test="${thumbnailExists == true}">
			<div class="form-group">
				<label for="thumbnail">Thumbnail:</label>
				<div class="d-flex justify-content-center" style="background-color:#343a40;border:1px solid #fff;border-radius:0.25rem;">
					<img src="${boardThumbnail.path}" class="rounded-lg" style="width:464px;height:286px;margin:10px;object-fit:cover;">
				</div>
			</div>
		</c:if>
			<div class="form-group">
				<input type="hidden" id="no" name="no" value="${board.no}"/>
				<label for="user">Writer:</label>
				<input type="text" class="form-control" id="writer" name="writer" value="${board.writer}" readonly="true" style="background-color: #343a40;color:#fff;"/>
			</div>

			<div class="form-group">
				<label for="subject">Subject:</label>
				<input type="text" class="form-control" id="subject" name="subject" value="${board.subject}" style="background-color: #343a40;color:#fff;" readonly="true" />
			</div>

			<div class="form-group">
				<label for="body">Body:</label>
				<textarea class="form-control" rows="5" id="body" name="body" readonly="true"
					style="background-color: #343a40;color:#fff;resize: none;">${board.body}</textarea>
			</div>
			
		<c:if test="${uploadFileExists == true}">
			<div class="form-group">
				<div class="fileArea">
					<c:choose>
						<c:when test="${isLogin == true}">
					<p style="margin:0;padding:0;"><span style="font-size:10px;color:rgba(255,255,255,.4);"><span style="color:firebrick;">*</span>ie는 다운로드를 지원하지 않습니다.</span></p>
					<span>💾<a href = "${boardUploadFile.uploadPath}" target="_blank" download>${boardUploadFile.originName}</a></span>
						</c:when>
						<c:otherwise>
					<span>💾<a href = "#" onClick='needLogin()'>${boardUploadFile.originName}</a></span>
						</c:otherwise>
					</c:choose>
				</div>
			</div>				
		</c:if>

			<div class="form-group">
				<label for="password">Password:</label> <label for="check"></label>
				<input type="password" class="form-control" id="master" name="master" style="background-color: #343a40;color:#fff;"/>
				<input type="hidden" id="boardMaster" name="boardMaster" value="${board.master}"/>
			</div>

			<div class="form-group"
				style="display: flex; justify-content: flex-end;">
				<input type="submit" class="btn btn-primary" id="modifyBtn" value="modify" disabled="true"/>&nbsp;
				<input type="button" class="btn btn-danger" id="deleteBtn" value="delete" disabled="true"/>&nbsp;
				<input type="button" class="btn btn-warning" onclick="location.href='<%=request.getContextPath()%>/list'" value="go List"/>
			</div>
		</div>
	</form>
	<form id="commentSave">
		<div class="container" style="padding-top: 100px;">
			<h3>Comments</h3>
			<p>
				There are <span style="color: firebrick; font-weight: 700;" id="commentCnt">${commentCount}</span>
				Comments.
			</p>

			<table class="table table-hover table-light">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Comment</th>
					</tr>
				</thead>
				<tbody id="commentTable">
				</tbody>
			</table>
		</div>
	</form>
	
	<div class="container">
		<input type="hidden" id="boardNo" name="boardNo" value="${board.no}"/>
		<div class="form-group">
			<textarea class="form-control" rows="3" id="cmtBody" name="cmtBody" style="background-color: #343a40;color:#fff;resize:none;" maxlength="50"></textarea>
		</div>

		<div class="form-group" style="display:flex;justify-content:flex-end;">
			<input type="submit" class="btn btn-success" id="commentBtn" value="write" disabled="true" onclick="commentWrite(${board.no})"/>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<!-- BootStrap -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  
	<!-- md5 -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.10.0/js/md5.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/JS/creative.min.js"></script>
  
	<script src="resources/JS/detail.js"></script>
	<script>
	
	function getContextPath() {
		var hostIndex = location.href.indexOf( location.host ) + location.host.length;
		return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
	}
	
	function showCommentList(root){
	    var url = getContextPath() + '/getCommentRoot?root=' + root;
	    var id = '${member.id}';

	    $.ajax({
	        url: url,
	        type: 'GET',
	        contentType: "charset=utf-8;",
	        dataType: "json",
	        success: function(result){
				if(result.length > 0){
					for(var i = 0; i < result.length; i++){
						if(id != result[i].writer){
							$("#commentTable").append("<tr><td>" + result[i].writer + "</td><td>" + result[i].body + "</td></tr>");
						}
						else{
							$("#commentTable").append("<tr id=\"comment" + result[i].no + "\"><td>" + result[i].writer + " <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentUpdate(" + result[i].no + ")\">수정</a> <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentDelete(" + result[i].no + ")\">삭제</a> </td><td>" + result[i].body + "</td></tr>");
						}
					}
				}
	        } // ajax success end
	    }); // ajax end
	}
	
	// 1. 댓글을 작성하면 controller에서 service를 통해 table에 데이터를 삽입하고 삽입한 데이터를 반환한다.
	// 2. controller에서 success로 리턴될 시 #commentTable에 append() 를 통해 새로 추가한 데이터를 추가한다.

	function commentWrite(root){
	    var url = getContextPath() + '/commentWrite?root=' + root;
	    var id = '${member.id}';
	    var paramData = JSON.stringify({
	        "root" : root,
	        "writer" : id,
	        "body" : $("#cmtBody").val()
	    });

	    $.ajax({
	        url: url,
	        type: 'POST',
	        contentType: 'application/json;charset=UTF-8',
	        data: paramData,
	        dataType: 'json',
	        success: function(result){
	            if(!result){
	                return;
	            }

	            if(result.writer != id){
	                $("#commentTable").append("<tr><td>" + result.writer + "</td><td>" + result.body + "</td></tr>");
	            }
	            else{
	                $("#commentTable").append("<tr id=\"comment" + result.no + "\"><td>" + result.writer + " <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentUpdate(" + result.no + ")\">수정</a> <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentDelete(" + result.no + ")\">삭제</a> </td><td>" + result.body + "</td></tr>");
	            }

	            // comment Count 1 Add
	            var cnt = $("#commentCnt").html();
	            cnt *= 1; // String to Number
	            cnt++;
	            $("#commentCnt").html(cnt);
	            
	            // for textarea initial
	            $("#cmtBody").val("");
	        },
	        error: function(error){
	            console.log("ERROR!");
	            console.log(error);
	        }
	    });
	}
	
	function commentUpdate(commentNo){
		var url = getContextPath() + '/getCommentNo?no=' + commentNo;
		var id = '${member.id}';
		
		$.ajax({
			url: url,
			type: 'GET',
			contentType: "charset=utf-8;",
			dataType: "json",
			success: function(result){

                if(!result){
                	return;
                }
                
				$("#comment" + commentNo).replaceWith("<tr id=\"comment" + result.no + "\"><td>" + result.writer + " <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentSave(" + result.no + ")\">저장</a> <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentCancel(" + result.no + ")\">취소</a></td><td><textarea class=\"form-control\" rows=\"3\" id=\"editComment" + result.no + "\" name=\"editComment" + result.no + "\" style=\"background-color: #343a40;color:#fff;resize:none;\">" + result.body + "</textarea></td></tr>");
			}
		});
	}
	
	function commentCancel(commentNo){
	    // 테이블 내 데이터 지우기
	    $('#table > tbody > #comment' + commentNo).empty();

	    var url = getContextPath() + '/getCommentNo?no=' + commentNo;
	    var id = '${member.id}';

	    $.ajax({
	        url: url,
	        type: 'GET',
	        contentType: "charset=utf-8;",
	        dataType: "json",
	        success: function(result){
	            if(!result){
	                return;
	            }
	            
	            if(result.writer != id){
	            	$("#comment" + commentNo).replaceWith("<tr><td>" + result.writer + "</td><td>" + result.body + "</td></tr>");
	            }
	            else{
	            	$("#comment" + commentNo).replaceWith("<tr id=\"comment" + result.no + "\"><td>" + result.writer + " <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentUpdate(" + result.no + ")\">수정</a> <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentDelete(" + result.no + ")\">삭제</a> </td><td>" + result.body + "</td></tr>");
	            }
	        } // success end
	    }); // ajax end
	}
	
	function commentSave(commentNo){
	    // InputTextArea: editComment[commentNo]
	    var url = getContextPath() + '/setCommentNo';
	    var id = '${member.id}';

	    var editComment = $('#editComment' + commentNo).val();
	    var paramData = JSON.stringify({
	            "no": commentNo,
	            "body": editComment
	        });

	    $.ajax({
	        url: url,
	        contentType: 'application/json;charset=UTF-8',
	        data: paramData,
	        type: 'POST',
	        dataType: 'json',
	        success: function(result){
	            if(result.writer != id){
	                $("#comment" + commentNo).replaceWith("<tr><td>" + result.writer + "</td><td>" + result.body + "</td></tr>");
	            }
	            else{
	                $("#comment" + commentNo).replaceWith("<tr id=\"comment" + result.no + "\"><td>" + result.writer + " <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentUpdate(" + result.no + ")\">수정</a> <a href=\"javascript:void(0);\" class=\"cmtLink\" onclick=\"commentDelete(" + result.no + ")\">삭제</a> </td><td>" + result.body + "</td></tr>");
	            }
	        },
	        error: function(error){
	        	alert('댓글을 정확히 입력해 주세요.');
	        	console.log(error);
	        }
	    });
	}
	
	function commentDelete(commentNo){
	    var result = confirm("댓글을 정말 삭제하시겠습니까?");
	    if(result){
	        var url = getContextPath() + '/commentDelete';
	        var paramData = JSON.stringify({ "no": commentNo });

	        $.ajax({
	            url: url,
	            contentType: 'application/json;charset=UTF-8',
	            data: paramData,
	            type: 'POST',
	            dataType: 'json',
	            success: function(bool){
	                if(bool == true){
	                    $("#comment" + commentNo).empty();
	                    
	                    var cmtCnt = $("#commentCnt").html();
	                    cmtCnt = cmtCnt - 1;

	                    $("#commentCnt").html(cmtCnt);
	                }
	                else{
	                    alert('댓글을 삭제할 수 없습니다.');
	                }
	            },
	            error: function(error){
	            	console.log("ERROR!");
	            	console.log(error);
	            }
	        });
	    }
	}
	
	function needLogin(){
		alert('로그인이 필요합니다.');
	}
	
	showCommentList('${board.no}');
	
	</script>
	
</body>
</html>
