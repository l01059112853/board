$(document).ready(function(){
	$("#id").on("propertychange change keyup paste input", function() {
		if($("#id").val() == "" || $("#pw").val() == "") {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled","disabled");
		}
		else {
			$("#submitBtn").prop("disabled", false);
			$("#submitBtn").removeAttr("disabled");
		}
	});

	$("#pw").on("propertychange change keyup paste input", function() {
		if($("#id").val() == "" || $("#pw").val() == "") {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled","disabled");
		}
		else {
			$("#submitBtn").prop("disabled", false);
			$("#submitBtn").removeAttr("disabled");
		}
	});	
});