function getContextPath() {
	var hostIndex = location.href.indexOf( location.host ) + location.host.length;
	return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
}

function search(){
	var searchInput = $("#searchInput").val();
	searchInput = $.trim(searchInput);
	var url = getContextPath() + '/search?search=' + encodeURI(searchInput);
	
	$.ajax({
		url: url,
		contentType: 'charset=UTF-8',
		type: 'GET',
		dataType: 'json',
		success: function(result){
			if(!result){
				alert('검색하신 단어 혹은 문장과 관련된 게시글이 존재하지 않습니다.');
				return;
			}
			else{
				if(searchInput == '' || searchInput == null || searchInput == undefined)
					$(location).attr('href', getContextPath() + '/list');
				else
					$(location).attr('href', getContextPath() + '/list?search=' + encodeURI(searchInput));
			}
		},
		error: function(error){
			console.log("ERROR!!!");
			console.log(error);
		}
	});
}

$(document).ready(function(){
	$("#searchInput").keydown(function(){
		if(event.keyCode == 13){
			event.preventDefault(); // disappear Enter Event
			search();
		}
	});
});