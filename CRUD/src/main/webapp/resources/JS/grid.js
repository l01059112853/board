function getContextPath() {
	var hostIndex = location.href.indexOf( location.host ) + location.host.length;
	return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
}

function search(){
	var searchInput = $("#searchInput").val();
	searchInput = $.trim(searchInput);
	var url = getContextPath() + '/search?search=' + encodeURI(searchInput);
	
	$.ajax({
		url: url,
		contentType: 'charset=UTF-8',
		type: 'GET',
		dataType: 'json',
		success: function(result){
			if(!result){
				alert('검색하신 단어 혹은 문장과 관련된 게시글이 존재하지 않습니다.');
				return;
			}
			else{
				if(searchInput == '' || searchInput == null || searchInput == undefined)
					$(location).attr('href', getContextPath() + '/grid');
				else
					$(location).attr('href', getContextPath() + '/grid?search=' + encodeURI(searchInput));
			}
		},
		error: function(error){
			console.log("ERROR!!!");
			console.log(error);
		}
	});
}

function getBoard(search) {
	var url;
	var gscSum = $("#gscSum").val();
	
	if(gscSum == undefined)
		gscSum = 0;
		
	if(search == '' || search == undefined)
		url = getContextPath() + '/seeMore?limitStart=' + $("#gscSum").val();
	else
		url = getContextPath() + '/seeMore?search=' + search + '&limitStart=' + $("#gscSum").val();
	
	$.ajax({
		url: url,
		contentType: 'charset=UTF-8',
		type: 'GET',
		dataType: 'json',
		success: function(result){
			
			if(result.status == false){
				alert('더 이상 조회할 게시글이 존재하지 않습니다.');
				return;
			}
			
			var html = '';
			for(var i = 0; i < result.board.length; i++){
			    if(i % 3 == 0)
			        html += '   <div class=\"board_lists rowMedia\">\n';

			    	html += '       <div class=\"col-xs-3 colMedia\">\n';
			    	html += '           <div class=\"boardThumbnail\">\n';
			    	html += '               <a href=\"' + getContextPath() + '/detail?no=' + result.board[i].no + '\">\n';
			    	html += '                   <img src=\"' + result.boardThumbnail[i] + '\" alt=\"\" class=\"thumbnailImage\">\n';

			    	html += '                   <div class=\"properties\">\n';
			    	html += '                       <div class=\"property\">\n';
			    	html += '                           💬' + result.boardCmt[i];

			    	if(result.boardUploadFile[i] == true)
			    		html += '&nbsp;&nbsp;&nbsp;🔗';

			    	html += '                       </div>';
			    	html += '                   </div>';
			    	html += '               </a>';
			    	html += '           </div>';
			    	html += '       </div>';

			    if(i % 3 == 2)
			    	html += '   </div>';

			}
			
			$(".container-fluid").append(html);
			$("#gscSum").val(result.gridScreenCount);
		},
		error: function(error){
			console.log("ERROR!!!");
			console.log(error);
		}
	});
}

$(document).ready(function() {
	
	var width = $('#firstImage').width();
	$('.thumbnailImage').height(width);
	
	//imageInitial();
	var documentHeight = $(document).height();
	
	$(window).resize(function() {
		var width = $('#firstImage').width();
		$('.thumbnailImage').height(width);
	});
	
	$("#remoteBtn").click(function(event) {
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
	});
	
	$("#searchInput").keydown(function(){
		if(event.keyCode == 13){
			event.preventDefault(); // disappear Enter Event
			search();
		}
	});
});