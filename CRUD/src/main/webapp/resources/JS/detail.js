$(document).ready(function(){
	$("#master").on("propertychange change keyup paste input", function(){
		
		if($("#master").val() == ""){
			$("#modifyBtn").prop("disabled", true);
			$("#modifyBtn").attr("disabled", "disabled");
			
			$("#deleteBtn").prop("disabled", true);
			$("#deleteBtn").attr("disabled", "disabled");
		}
		else{
			$("#modifyBtn").prop("disabled", false);
			$("#modifyBtn").removeAttr("disabled");
			
			$("#deleteBtn").prop("disabled", false);
			$("#deleteBtn").removeAttr("disabled");
		}
	});
	
	$('#modifyBtn').click(function(){
		$("#detailForm").action('modify?no=${board.no}');
		$("#detailForm").method('post');
		$("#detailForm").submit();
	});
	
	
	$("#cmtBody").on("propertychange change keyup paste input", function(){
		if($("#cmtBody").val() == ""){
			$("#commentBtn").prop("disabled", true);
			$("#commentBtn").attr("disabled", "disabled");
		}
		else {
			$("#commentBtn").prop("disabled", false);
			$("#commentBtn").removeAttr("disabled");			
		}
	});
	
	$('#deleteBtn').click(function(){
		var boardMaster = $("#boardMaster").val();
		var inputMaster = md5($("#master").val());
		
		if(boardMaster == inputMaster){
			
			var result = confirm("게시글을 정말로 삭제하시겠습니까?");
			
			if(result){
				var no = $("#no").val();
				var writer = $("#writer").val();
				$(location).attr('href', getContextPath() + '/delete?no=' + no + '&writer=' + writer);
			}
		}
	});
	
	function getContextPath() {
		var hostIndex = location.href.indexOf( location.host ) + location.host.length;
		return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
	}
	
	
});