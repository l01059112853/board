$(document).ready(function(){
	$("#subject").on("propertychange change keyup paste input", function() {
		if($("#subject").val() == "" || $("#body").val() == "") {
			$("#modifyBtn").prop("disabled", true);
			$("#modifyBtn").attr("disabled","disabled");
		}
		else {
			$("#modifyBtn").prop("disabled", false);
			$("#modifyBtn").removeAttr("disabled");
		}
	});

	$("#body").on("propertychange change keyup paste input", function() {
		if($("#subject").val() == "" || $("#body").val() == "") {
			$("#modifyBtn").prop("disabled", true);
			$("#modifyBtn").attr("disabled","disabled");
		}
		else {
			$("#modifyBtn").prop("disabled", false);
			$("#modifyBtn").removeAttr("disabled");
		}
	});
	
	var fileTarget = $('#boardFile');
	var thumbnailTarget = $('#boardThumbnail');
	
	fileTarget.on('change', function(UPLOAD_PATH){
		var filename;
		var originname;
		
		if(window.FileReader) {
			filename = $(this)[0].files[0].name;
		}
		else {
			filename = $(this).val().split('/').pop().split('\\').pop();
		}
		
		$('#boardFileLabel').html(filename);
	});
	
	thumbnailTarget.on('change', function(){
		var thumbname;
		var originname;
		
		if(window.FileReader){
			filename = $(this)[0].files[0].name;
		}
		else{
			filename = $(this).val().split('/').pop().split('//').pop();
		}
		
		$('#boardThumbnailLabel').html(filename);
	});
});