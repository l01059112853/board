	function clock(){
		var date = new Date();
		var day = date.getDay();

		var hours = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();

		document.getElementById("hours").innerText = `${hours}`;
		document.getElementById("minutes").innerText = `${minutes}`;
		document.getElementById("seconds").innerText = `${seconds}`;
	}

	function init(){
		clock();
		setInterval(clock, 1000);
	}

	init();