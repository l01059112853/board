$(document).ready(function(){

	$("#subject").on("propertychange change keyup paste input", function() {
		if($("#subject").val() == "" || $("#body").val() == "" || $("#master").val() == "") {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled","disabled");
		}
		else {
			$("#submitBtn").prop("disabled", false);
			$("#submitBtn").removeAttr("disabled");
		}
	});

	$("#body").on("propertychange change keyup paste input", function() {
		if($("#subject").val() == "" || $("#body").val() == "" || $("#master").val() == "") {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled","disabled");
		}
		else {
			$("#submitBtn").prop("disabled", false);
			$("#submitBtn").removeAttr("disabled");
		}
	});
	
	$("#master").on("propertychange change keyup paste input", function() {
		if($("#subject").val() == "" || $("#body").val() == "" || $("#master").val() == "") {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled","disabled");
		}
		else {
			$("#submitBtn").prop("disabled", false);
			$("#submitBtn").removeAttr("disabled");
		}
	});
	
	var fileTarget = $('#boardFile');
	var thumbnailTarget = $('#boardThumbnail');
	
	fileTarget.on('change', function(UPLOAD_PATH){
		var filename;
		var originname;
		
		if(window.FileReader) {
			filename = $(this)[0].files[0].name;
		}
		else {
			filename = $(this).val().split('/').pop().split('\\').pop();
		}
		
		$('#boardFileLabel').html(filename);
	});
	
	thumbnailTarget.on('change', function(){
		var thumbname;
		var originname;
		
		if(window.FileReader){
			filename = $(this)[0].files[0].name;
		}
		else{
			filename = $(this).val().split('/').pop().split('//').pop();
		}
		
		$('#boardThumbnailLabel').html(filename);
	});
});

function getContextPath() {
	var hostIndex = location.href.indexOf( location.host ) + location.host.length;
	return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
}