$(document).ready(function(){
	var flag = false;
	
    $("#id").on("propertychange change keyup paste input", function() {
        var url = getContextPath() + '/signUpValidate';
        var inputVal = $("#id").val();

        if(inputVal.length >= 4) {
            $.ajax({
                url: url,
                contentType: 'application/json;charset=UTF-8',
                data: JSON.stringify({ "id": inputVal }),
                type: 'POST',
                dataType: 'json',
                success: function(bool) {

                    if(bool == true) {
                    	flag = true;
                        $("#idError").html('😊 사용 <span style="color:red;">가능한</span> 아이디 입니다.');
                    }
                    else {
                        flag = false;
                        $("#idError").html('🤔 사용할 수 <span style="color:red;">없는</span> 아이디 입니다.');
                    }
                }
            });
        }
        else {
            flag = false;
            $("#idError").html('🤔 사용할 수 <span style="color:red;">없는</span> 아이디 입니다.');
        }
    
        signUpValidate();
    });

	$("#pw").on("propertychange change keyup paste input", function() {
		signUpValidate();
	});
	
	$("#name").on("propertychange change keyup paste input", function() {
		signUpValidate();
	});
	
	function signUpValidate() {	
		if(flag == false) {
			$("#submitBtn").prop("disabled", true);
			$("#submitBtn").attr("disabled", "disabled");
		}
		else {
			if($("#id").val() == "" || $("#pw").val() == "" || $("#name").val() == "") {
				$("#submitBtn").prop("disabled", true);
				$("#submitBtn").attr("disabled", "disabled");
			}
			else{
				$("#submitBtn").prop("disabled", false);
				$("#submitBtn").removeAttr("disabled");
			}
		}
	}
});

function getContextPath() {
	var hostIndex = location.href.indexOf( location.host ) + location.host.length;
	return location.href.substring( hostIndex, location.href.indexOf('/', hostIndex + 1) );
}