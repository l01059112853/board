package com.minh.board.DTO;

public class commentDTO {

	int no, root, subroot;
	String writer, body, date;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getRoot() {
		return root;
	}
	public void setRoot(int root) {
		this.root = root;
	}
	public int getSubroot() {
		return subroot;
	}
	public void setSubroot(int subroot) {
		this.subroot = subroot;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "commentDTO [no=" + no + ", root=" + root + ", subroot=" + subroot + ", writer=" + writer + ", body="
				+ body + ", date=" + date + "]";
	}
}
