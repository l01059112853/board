package com.minh.board.DTO;

public class boardThumbnailDTO {

	int no;
	String path;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	@Override
	public String toString() {
		return "boardThumbnailDTO [no=" + no + ", path=" + path + "]";
	}
}
