package com.minh.board.DTO;

public class boardDTO {
	int no;
	String writer, subject, body, date, master;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	
	@Override
	public String toString() {
		return "boardDTO [no=" + no + ", writer=" + writer + ", subject=" + subject + ", body=" + body + ", date="
				+ date + ", master=" + master + "]";
	}
	
	
}
