package com.minh.board.DTO;

public class memberProfileDTO {
	String id, path;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "memberProfileDTO [id=" + id + ", path=" + path + "]";
	}
	
}
