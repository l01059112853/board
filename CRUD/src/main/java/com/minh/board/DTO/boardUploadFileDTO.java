package com.minh.board.DTO;

public class boardUploadFileDTO {
	private int no;
	private String originName, uploadPath;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getOriginName() {
		return originName;
	}
	public void setOriginName(String originName) {
		this.originName = originName;
	}
	public String getUploadPath() {
		return uploadPath;
	}
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
	
	@Override
	public String toString() {
		return "boardUploadFileDTO [no=" + no + ", originName=" + originName + ", uploadPath=" + uploadPath + "]";
	}
}
