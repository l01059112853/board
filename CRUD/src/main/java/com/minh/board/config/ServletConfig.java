package com.minh.board.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@ComponentScan(basePackages= {"com.minh.board.controller" })
@PropertySource({"classpath:profiles/${spring.profiles.active}/application.properties"})
public class ServletConfig implements WebMvcConfigurer{
	
	@Value("${upload.location}")
	String UPLOAD_LOCAL;
	
	@Value("${upload.uri_path}")
	String UPLOAD_PATH;

	static final Logger logger = LoggerFactory.getLogger(ServletConfig.class);
	static final int MAX_SIZE = 30 * 1024 * 1024;
	
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("mypage/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler(UPLOAD_PATH + "/**").addResourceLocations("file://" + UPLOAD_LOCAL);
	}
	
	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(MAX_SIZE);
		resolver.setMaxUploadSizePerFile(MAX_SIZE);
		resolver.setMaxInMemorySize(0);
		return resolver;
	}
}
