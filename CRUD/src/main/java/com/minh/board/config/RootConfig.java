package com.minh.board.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration // @Configuration 어노테이션은 해당 클래스가 설정 파일임을 알려준다.
@ComponentScan(basePackages = {"com.minh.board"}) // @ComponentScan 어노테이션은 basePackages에 설정된 패키지가 빈이라는 것을 알려준다.
@MapperScan(basePackages= {"com.minh.board.DAO"}) // @MapperScan 어노테이션은 mybatis Mapper를 연결시키기 위함이다.

@EnableScheduling
@EnableTransactionManagement
public class RootConfig {

	@Bean
	public DataSource dataSource() {
		DataSource ds = new DataSource();
		
		// log4jdbc를 통해 SQL쿼리문이 Console에 나타내도록 하기 위한 설정
		ds.setDriverClassName("net.sf.log4jdbc.sql.jdbcapi.DriverSpy");
		ds.setUrl("jdbc:log4jdbc:mariadb://13.125.54.41:3306/board?serverTimezone=Asia/Seoul");
		ds.setUsername("minh");
		ds.setPassword("1234");
		ds.setInitialSize(2);
		ds.setMaxActive(10);
		ds.setMaxIdle(10);
		
		// ValidationQuery
		// 일정시간(디폴트 8hours)마다 DB에 요청이 없으면 커넥션을 자동으로 끊어버리게 된다
		// 이를 예방하기 위해 특정 시간별로 요청을 줌으로써 커넥션이 끊기지 않도록 만든다.

		ds.setTestWhileIdle(true);
		ds.setValidationQuery("select 1");
		ds.setMinEvictableIdleTimeMillis(60000*3);
		ds.setTimeBetweenEvictionRunsMillis(10*1000);
		ds.setValidationQueryTimeout(2880);
		
		return ds;
	}
	
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource());
		
		return (SqlSessionFactory)sqlSessionFactory.getObject();
	}
}
