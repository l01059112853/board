package com.minh.board.util;

import java.util.HashMap;

public class paging {
	
	public static HashMap<String, Integer> get(int totalBoard, int screenPage, int screenBoard, int page){
		HashMap<String, Integer> map = new HashMap<>();
		
		// 페이지 맨 좌측부와 맨 우측부가 저장될 변수
		int head = 0, tail = 0;
		
		// 총 페이지 개수
		int totalPage = totalBoard / screenBoard;
		
		// 게시글의 총 수에 화면에 표시될 게시글 수를 나눴을 때 나머지 값이 존재할 경우 한 페이지를 더 구현해야 하므로 totalPage의 값을 1 증가시킨다.
		if(totalBoard % screenBoard > 0)
			totalPage++;
		
		// 현재 페이지가 토탈페이지 수보다 높을 경우 현재 페이지 값을 토탈 페이지 값으로 변경한다.
		// 오류 예방을 위한 처리
		if(page > totalPage)
			page = totalPage;
		
		/* 소괄호 안에서 1을 뺀 이유
		 * screenPage가 10이라고 칠 시 page가 10인 경우 이 과정을 거치게 되면 10/10 * 10 이 되어 10이된다. 즉 헤드가 10이되어 버린다.
		 * 따라서 1을 깎음으로써 9 / 10 = 0 * 10 = 0 이 된다.
		 * 마지막에 1을 더한 이유는 페이지는 0이 아니라 1부터 시작하기 때문이다.
		 */
		head = ((page - 1) / screenPage) * screenPage + 1;
		
		/*
		 * 1을 뺀 이유
		 * 페이지 1에서 screenPage가 10일 경우 11이 되기 때문에 1을 빼줘야 한다.
		 */
		tail = head + screenPage - 1;
		
		// 하지만 tail이 totalPage를 넘어서는 안되기 때문에 값을 조정한다.
		if(tail > totalPage)
			tail = totalPage;
		
		map.put("totalBoard", totalBoard); // 최대 게시글 수
		map.put("totalPage", totalPage); // 최대 페이지 값
		map.put("screenPage", screenPage); // 화면에 나타낼 최대 페이지 수
		map.put("screenBoard", screenBoard); // 화면에 나타낼 최대 게시글 수
		map.put("currentPage", page); // 현재 페이지
		map.put("currentHead", head); // 현재 페이지 시작 값
		map.put("currentTail", tail); // 현재 페이지 마지막 값
		
		return map;
	}
}
