package com.minh.board.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class util {

	public static String timeConvert(String date, String timeFormat) {
		String format = timeFormat;
		String returnTime = "";
		
		String[] splitArr = date.split(" ");
		String[] YMDArr = splitArr[0].split("-");
		String[] HMSArr = splitArr[1].split(":");
		
		// 2020-01-27 05:40:55
		
		if(format == "YYMMDD HHMM")
			returnTime = YMDArr[0].substring(2) + "." + YMDArr[1] + "." + YMDArr[2] + " " + HMSArr[0] + ":" + HMSArr[1];
		else if(format == "YYMMDD")
			returnTime = YMDArr[0].substring(2) + "." + YMDArr[1] + "." + YMDArr[2];
		else if(format == "HHMMSS")
			returnTime = HMSArr[0] + ":" + HMSArr[1] + ":" + HMSArr[2];
		
		return returnTime;
	}
	
	// 바이트 배열을 HEX 문자열로 변환
	// 바이트 배열을 16진수 문자열로 반환한다.
	public static String byteToHexString(byte[] data) {
	    StringBuilder sb = new StringBuilder();
	    for(byte b : data) {
	        sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
	    }
	    return sb.toString();
	}
	
	// 입력된 값을 md5로 해서 만든다.
	public static String md5(String pw) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(pw.getBytes());
		return byteToHexString(md.digest());
	}
	
	public static String htmlspecialchars(String str) {
		String[] replaceArr = { "&", "\"", "'", "<", ">" };
		String[] changeArr = { "&amp;", "&quot;", "&#039;", "&lt;", "&gt;" };
		
		for(int i = 0; i < 5; i++)
			str = str.replace(replaceArr[i], changeArr[i]);
		
		return str;
	}
}
