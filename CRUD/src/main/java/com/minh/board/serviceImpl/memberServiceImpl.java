package com.minh.board.serviceImpl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.memberDAO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.service.memberService;

@Service
public class memberServiceImpl implements memberService {
	private static final Logger logger = LoggerFactory.getLogger(memberService.class);
	
	@Autowired
	memberDAO memberDAO;

	@Override
	public boolean login(memberDTO memberDTO, HttpSession session) throws Exception {
		boolean result = false;
		
		if(memberDTO == null)
		{
			logger.info(memberDTO.toString() + " : memberDTO is null");
			return result;
		}
		
		if(memberDTO.getId() == null || memberDTO.getPw() == null)
		{
			logger.info(memberDTO.toString() + " : memberDTO is not null. but id or pw properties is null.");
			return result;
		}
		
		memberDTO selectMember = memberDAO.selectIdPw(memberDTO);
		
		if(selectMember == null)
		{
			logger.info(selectMember.toString() + " : selectMember is null. because id or pw is not correctly.");
			return result;
		}
		
		logger.info(selectMember.toString() + " : login success.");
		
		session.setAttribute("member", selectMember); // save data who be login in session.
		session.setAttribute("loginStatus", "login"); // loginStatus is login.
		result = true;
		
		return result;
	}

	@Override
	public void logout(HttpSession session) throws Exception {
		
		logger.info("logout success.");
		
		session.setAttribute("member", null);
		session.setAttribute("loginStatus", "logout");
		session.invalidate(); // for session expire
	}

	@Override
	public boolean isLogin(HttpSession session) throws Exception {
		
		boolean result;
		
		if(session.getAttribute("member") == null)
			result = false;
		else
			result = true;
		
		return result;
	}

	@Override
	public void insert(memberDTO memberDTO) throws Exception {
		memberDAO.insert(memberDTO);
	}

	@Override
	public void update(memberDTO memberDTO) throws Exception {
		memberDAO.update(memberDTO);
	}

	@Override
	public List<memberDTO> select() throws Exception {
		List<memberDTO> memberList = memberDAO.select();
		return memberList;
	}

	@Override
	public memberDTO selectIdPw(memberDTO memberDTO) throws Exception {
		
		if(memberDTO == null)
			return null;
		
		String id = memberDTO.getId();
		String pw = memberDTO.getPw();
		
		if(id == "" || pw == "")
			return null;
		
		memberDTO dto = new memberDTO();
		dto.setId(id);
		dto.setPw(pw);
		
		memberDTO vo = memberDAO.selectIdPw(dto);
		if(vo == null)
			return null;
		
		if(vo.getId() == "" || vo.getPw() == "")
			return null;

		return vo;
	}

	@Override
	public memberDTO selectSession(HttpSession session) throws Exception {
		
		memberDTO memberDTO = null;
		
		if(session.getAttribute("member") != null)
			memberDTO = (memberDTO)session.getAttribute("member");
		
		return memberDTO;
	}

	@Override
	public memberDTO selectId(memberDTO memberDTO) throws Exception {
		
		if(memberDTO == null)
			return null;
		
		return memberDAO.selectId(memberDTO);
	}
}
