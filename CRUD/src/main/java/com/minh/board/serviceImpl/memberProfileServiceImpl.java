package com.minh.board.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.memberProfileDAO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.DTO.memberProfileDTO;
import com.minh.board.service.memberProfileService;

@Service
public class memberProfileServiceImpl implements memberProfileService {
	
	@Autowired
	memberProfileDAO memberProfileDAO;

	@Override
	public List<memberProfileDTO> select() throws Exception {
		return memberProfileDAO.select();
	}

	@Override
	public memberProfileDTO selectId(memberDTO memberDTO) throws Exception {
		// TODO Auto-generated method stub
		return memberProfileDAO.selectId(memberDTO);
	}

	@Override
	public void insert(memberProfileDTO memberProfileDTO) throws Exception {
		memberProfileDAO.insert(memberProfileDTO);
	}

	@Override
	public void update(memberProfileDTO memberProfileDTO) throws Exception {
		memberProfileDAO.update(memberProfileDTO);
	}

}
