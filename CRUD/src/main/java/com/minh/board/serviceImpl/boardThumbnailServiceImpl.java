package com.minh.board.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.boardThumbnailDAO;
import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardThumbnailDTO;
import com.minh.board.service.boardThumbnailService;

@Service
public class boardThumbnailServiceImpl implements boardThumbnailService {
	
	@Autowired
	boardThumbnailDAO boardThumbnailDAO;

	@Override
	public boardThumbnailDTO selectNo(boardDTO boardDTO) throws Exception {
		return boardThumbnailDAO.selectNo(boardDTO);
	}

	@Override
	public void update(boardThumbnailDTO boardThumbnailDTO) throws Exception {
		boardThumbnailDAO.update(boardThumbnailDTO);
	}

}
