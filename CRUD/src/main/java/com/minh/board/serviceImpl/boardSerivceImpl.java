package com.minh.board.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.boardDAO;
import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.service.boardService;

@Service
public class boardSerivceImpl implements boardService {
	
	@Autowired
	boardDAO boardDAO;

	@Override
	public List<boardDTO> select(String where, int start, int end) throws Exception {
		return boardDAO.select(where, start, end);
	}

	@Override
	public boardDTO selectNo(int no) throws Exception {
		return boardDAO.selectNo(no);
	}
	
	@Override
	public int selectCount(String where) throws Exception {
		return boardDAO.selectCount(where);
	}
	
	@Override
	public List<boardDTO> selectMyPage(String memberID) throws Exception {
		return boardDAO.selectMyPage(memberID);
	}

	@Override
	public void insert(boardDTO boardDTO) throws Exception {
		boardDAO.insert(boardDTO);
	}

	@Override
	public void update(boardDTO boardDTO) throws Exception {
		boardDAO.update(boardDTO);
	}

	@Override
	public void delete(boardDTO boardDTO) throws Exception {
		boardDAO.delete(boardDTO);
	}
}
