package com.minh.board.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.commentDAO;
import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.service.commentService;

@Service
public class commentServiceImpl implements commentService {
	
	@Autowired
	commentDAO commentDAO;

	@Override
	public List<commentDTO> selectRoot(int root) throws Exception {
		return commentDAO.selectRoot(root); 
	}
	
	@Override
	public List<commentDTO> selectWriter(commentDTO commentDTO) throws Exception {
		return commentDAO.selectWriter(commentDTO);
	}

	@Override
	public commentDTO selectNo(int no) throws Exception {
		return commentDAO.selectNo(no);
	}

	@Override
	public commentDTO selectSubRoot(int subRoot) throws Exception {
		return commentDAO.selectSubRoot(subRoot);
	}
	
	@Override
	public int selectRootCount(int root) throws Exception {
		return commentDAO.selectRootCount(root);
	}

	@Override
	public void insert(commentDTO commentDTO) throws Exception {
		commentDAO.insert(commentDTO);
	}

	@Override
	public void update(commentDTO commentDTO) throws Exception {
		commentDAO.update(commentDTO);
	}

	@Override
	public void delete(commentDTO commentDTO) throws Exception {
		commentDAO.delete(commentDTO);
	}
}
