package com.minh.board.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minh.board.DAO.boardUploadFileDAO;
import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardUploadFileDTO;
import com.minh.board.service.boardUploadFileService;

@Service
public class boardUploadFileServiceImpl implements boardUploadFileService {
	
	@Autowired
	boardUploadFileDAO boardUploadFileDAO;

	@Override
	public boardUploadFileDTO selectNo(boardDTO boardDTO) throws Exception {
		return boardUploadFileDAO.selectNo(boardDTO);
	}

	@Override
	public void insert(boardUploadFileDTO boardUploadFileDTO) throws Exception {
		boardUploadFileDAO.insert(boardUploadFileDTO);
	}
	
	@Override
	public void update(boardUploadFileDTO boardUploadFileDTO) throws Exception {
		boardUploadFileDAO.update(boardUploadFileDTO);	
	}

	@Override
	public void delete(boardUploadFileDTO boardUploadFileDTO) throws Exception {
		boardUploadFileDAO.delete(boardUploadFileDTO);
	}
}
