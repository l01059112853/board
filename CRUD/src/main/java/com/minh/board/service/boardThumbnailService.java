package com.minh.board.service;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardThumbnailDTO;

public interface boardThumbnailService {
	public boardThumbnailDTO selectNo(boardDTO boardDTO) throws Exception;
	public void update(boardThumbnailDTO boardThumbnailDTO) throws Exception;
}
