package com.minh.board.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.memberDTO;

public interface boardService {
	public List<boardDTO> select(String where, int start, int end) throws Exception;
	public boardDTO selectNo(int no) throws Exception;
	public int selectCount(String where) throws Exception;
	public List<boardDTO> selectMyPage(String memberID) throws Exception;
	public void insert(boardDTO boardDTO) throws Exception;
	public void update(boardDTO boardDTO) throws Exception;
	public void delete(boardDTO boardDTO) throws Exception;
}
