package com.minh.board.service;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardUploadFileDTO;

public interface boardUploadFileService {
	public boardUploadFileDTO selectNo(boardDTO boardDTO) throws Exception;
	public void insert(boardUploadFileDTO boardUploadFileDTO) throws Exception;
	public void update(boardUploadFileDTO boardUploadFileDTO) throws Exception;
	public void delete(boardUploadFileDTO boardUploadFileDTO) throws Exception;
}
