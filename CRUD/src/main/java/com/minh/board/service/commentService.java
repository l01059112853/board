package com.minh.board.service;

import java.util.List;

import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;

public interface commentService {
	public List<commentDTO> selectRoot(int root) throws Exception;
	public List<commentDTO> selectWriter(commentDTO commentDTO) throws Exception;
	public commentDTO selectNo(int no) throws Exception;
	public commentDTO selectSubRoot(int subRoot) throws Exception;
	public int selectRootCount(int root) throws Exception;
	public void insert(commentDTO commentDTO) throws Exception;
	public void update(commentDTO commentDTO) throws Exception;
	public void delete(commentDTO commentDTO) throws Exception;
}
