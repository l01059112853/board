package com.minh.board.service;

import java.util.List;

import com.minh.board.DTO.memberDTO;
import com.minh.board.DTO.memberProfileDTO;


public interface memberProfileService {
	public List<memberProfileDTO> select() throws Exception;
	public memberProfileDTO selectId(memberDTO memberDTO) throws Exception;
	public void insert(memberProfileDTO memberProfileDTO) throws Exception;
	public void update(memberProfileDTO memberProfileDTO) throws Exception;
}
