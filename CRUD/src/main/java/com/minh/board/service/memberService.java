package com.minh.board.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.memberDTO;

public interface memberService {
	public boolean login(memberDTO memberDTO, HttpSession session) throws Exception;
	public void logout(HttpSession session) throws Exception;
	public boolean isLogin(HttpSession session) throws Exception;
	public List<memberDTO> select() throws Exception;
	public memberDTO selectId(memberDTO memberDTO) throws Exception;
	public memberDTO selectIdPw(memberDTO memberDTO) throws Exception;
	public memberDTO selectSession(HttpSession session) throws Exception;
	public void insert(memberDTO memberDTO) throws Exception;
	public void update(memberDTO memberDTO) throws Exception;
}
