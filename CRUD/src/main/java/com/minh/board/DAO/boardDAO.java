package com.minh.board.DAO;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.lang.Nullable;

import com.minh.board.DTO.boardDTO;

public interface boardDAO {
	public List<boardDTO> select(@Nullable @Param("where") String where, @Param("start") int start, @Param("end") int end) throws Exception;
	public boardDTO selectNo(@Param("boardNo") int no) throws Exception;
	public int selectCount(@Nullable @Param("where") String where) throws Exception;
	public List<boardDTO> selectMyPage(@Param("writer") String memberID) throws Exception;
	public void insert(boardDTO boardDTO) throws Exception;
	public void update(boardDTO boardDTO) throws Exception;
	public void delete(boardDTO boardDTO) throws Exception;
}
