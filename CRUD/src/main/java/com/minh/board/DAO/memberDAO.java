package com.minh.board.DAO;

import java.util.List;

import com.minh.board.DTO.memberDTO;

public interface memberDAO {
	public List<memberDTO> select() throws Exception;
	public memberDTO selectId(memberDTO memberDTO) throws Exception;
	public memberDTO selectIdPw(memberDTO memberDTO) throws Exception;
	public void insert(memberDTO memberDTO) throws Exception;
	public void update(memberDTO memberDTO) throws Exception;
	public void delete(memberDTO membdrDTO) throws Exception;
}
