package com.minh.board.DAO;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardThumbnailDTO;

public interface boardThumbnailDAO {
	public boardThumbnailDTO selectNo(boardDTO boardDTO) throws Exception;
	public void update(boardThumbnailDTO boardThumbnailDTO) throws Exception;
}
