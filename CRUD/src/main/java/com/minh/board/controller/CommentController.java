package com.minh.board.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.service.commentService;
import com.minh.board.service.memberService;
import com.minh.board.util.util;

@Controller
public class CommentController {
	
	private static final Logger logger = LoggerFactory.getLogger(CommentController.class); 

	@Autowired
	memberService memberService;
	
	@Autowired
	commentService commentService;
	
	@RequestMapping(value="getCommentRoot", method=RequestMethod.GET)
	@ResponseBody
	public List<commentDTO> getCommentRoot(HttpServletRequest request) throws Exception {
		
		String root = request.getParameter("root");
		if(root == null)
			return null;
		
		return commentService.selectRoot(Integer.parseInt(root));
	}
	
	@RequestMapping(value="getCommentNo", method=RequestMethod.GET)
	@ResponseBody
	public commentDTO getCommentNo(HttpServletRequest request) throws Exception {
		
		String no = request.getParameter("no");
		if(no == null)
			return null;
		
		return commentService.selectNo(Integer.parseInt(no));
	}
	
	@RequestMapping(value="setCommentNo", method=RequestMethod.POST)
	@ResponseBody
	public commentDTO setCommentNo(HttpServletRequest request, @RequestBody commentDTO commentDTO) throws Exception{
		
		//logger.info(commentDTO.toString());
		
		int no = commentDTO.getNo();
		String body = commentDTO.getBody();
		
		if(no == 0 || body.isEmpty())
			return null;
		
		commentDTO.setBody(util.htmlspecialchars(body)); // 댓글 내용 중 특정 문자(ex: html tag) 포함 시 내용이 수정되어야 함.
		
		commentService.update(commentDTO); // 댓글 업데이트 후
		return commentService.selectNo(no);
	}
	
	@RequestMapping(value="commentWrite", method=RequestMethod.POST)
	@ResponseBody
	public commentDTO commentWrite(HttpSession session, @RequestBody commentDTO commentDTO) throws Exception{
		
		if(commentDTO == null) {
			logger.info("commentDTO is null");
			return null;
		}
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == false) {
			logger.info("session is null");
			return null;
		}
		else {
			logger.info("session is not null. get loginData.");
			vo = memberService.selectSession(session);
		}
		
		if(commentDTO.getRoot() == 0 || commentDTO.getWriter() == "" || commentDTO.getBody() == "") {
			logger.info("data of commentDTO is empty.");
			return null;
		}
		
		// 댓글 내용 중 특정 문자(ex: html tag) 포함 시 내용이 수정되어야 함. 
		commentDTO.setBody(util.htmlspecialchars(commentDTO.getBody()));
		
		commentService.insert(commentDTO);
		// mybatis mapper에서 insert시 last_insert_id()를 가져와서 현재 dto에 삽입하게 되므로
		// 반환하더라도 commentDTO.getNo() 를 호출할 경우 방금 삽입한 comment의 no값을 구해올 수 있게 된다.
		return commentDTO;
	}
	
	@RequestMapping(value="commentDelete", method=RequestMethod.POST)
	@ResponseBody
	public boolean commentDeleteDo(HttpSession session, @RequestBody commentDTO commentDTO) throws Exception {
		
		memberDTO vo = null; // 로그인한 사용자의 데이터를 조회할 dto
		logger.info("vo initial.");
		
		if(memberService.isLogin(session) == false) {
			logger.info("session is null");
			return false;
		}
		else {
			vo = memberService.selectSession(session);
			logger.info("vo injection from memberService.");
		}
		
		// 댓글 번호로 조회한 해당 댓글의 데이터가 저장될 dto
		commentDTO cmtDTO = commentService.selectNo(commentDTO.getNo());
		logger.info("cmtDTO injection from commentService.");
		if(cmtDTO == null || cmtDTO.getNo() == 0) {
			logger.info("cmtDTO is null OR cmtDTO.getNo() is 0");
			return false;
		}
		
		// 댓글 번호에 해당되는 작성자와 현재 로그인한 사용자의 아이디가 다를 경우에는 댓글을 삭제할 수 없다.
		if(!vo.getId().equals(cmtDTO.getWriter())) {
			logger.info("vo.getId() is not equals cmt.getWriter()");
			return false;
		}
		
		logger.info("cmtDTO is delete.");
		commentService.delete(cmtDTO); // 댓글 삭제
		logger.info("return");
		return true; // 성공적으로 삭제했으니 true를 반환.
	}
}
