package com.minh.board.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.minh.board.DTO.memberProfileDTO;
import com.minh.board.service.memberProfileService;
import com.minh.board.service.memberService;

@Controller
@PropertySource({"classpath:profiles/${spring.profiles.active}/application.properties"})
public class UploadController {
	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);

	@Value("${upload.location}")
	String UPLOAD_PATH;
	
	@Value("${upload.uri_path}")
	String UPLOAD_URL;
	
	@Autowired
	memberService memberService;

	@Autowired
	memberProfileService memberProfileService;

	@RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadImage(Model model, HttpServletRequest request, HttpSession session, @RequestParam("uploadImage") MultipartFile multipartFile) throws IOException, Exception {
		
		// not login
		if(!memberService.isLogin(session)) {
			logger.info("not login currently");
			return "error: 로그인이 필요한 기능입니다.";
		}
		
		if(multipartFile.isEmpty()) {
			logger.info("upload file is not exist.");
			return "error: 파일이 선택되지 않았습니다.";
		}
		
		// directory
		File directory = new File(UPLOAD_PATH);
		// if not exists directory,
		if(!directory.exists()) {
			// make directory
			directory.mkdirs();
		}
		
		// file original name
		String originFileName = multipartFile.getOriginalFilename();
		// file extension
		String fileExtension = originFileName.substring(originFileName.lastIndexOf("."));
		// file naming (using UUID)
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		
		// get uploading fileData
		byte fileData[] = multipartFile.getBytes();
		logger.info("fileData: " + fileData);
		
		logger.info(UPLOAD_PATH + "\\" + uuid + fileExtension);
		FileOutputStream fileOutputStream = new FileOutputStream(UPLOAD_PATH + "/" + uuid + fileExtension);
		logger.info("fileData write.");
		fileOutputStream.write(fileData); // write fileOutputStream.
		
		if(fileOutputStream != null) {
			fileOutputStream.close(); // closing fileOutputStream.
		}
		
		//upload.location: C:/CRUD/Upload
		//upload.uri_path: /Upload
		
		String host = request.getRequestURL().toString().replace(request.getRequestURI(), "");
		String rtnPath = host + UPLOAD_URL + "/" + uuid + fileExtension;
		logger.info("PATH : " + rtnPath);
		
		memberProfileDTO memberProfileDTO = new memberProfileDTO();
		memberProfileDTO.setId(memberService.selectSession(session).getId());
		memberProfileDTO.setPath(rtnPath);
		
		memberProfileService.update(memberProfileDTO);
		
		return rtnPath;
	}
}
