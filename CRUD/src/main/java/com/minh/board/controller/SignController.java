package com.minh.board.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.minh.board.DTO.memberDTO;
import com.minh.board.service.memberService;

@Controller
public class SignController {
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	memberService memberService;
	
	@RequestMapping(value="/signin", method= {RequestMethod.GET, RequestMethod.POST})
	public String signIn(Model model, HttpServletRequest request, HttpSession session) throws Exception {
		
		return "signin";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public String signUp(Model model, HttpServletRequest request) {

		return "signup";
	}
	
	@RequestMapping(value="/logout", method={RequestMethod.GET, RequestMethod.POST})
	public String logout(HttpSession session) throws Exception {
		
		if(memberService.isLogin(session) == false) {
			return "redirect:/";
		}
		
		memberService.logout(session);
		return "redirect:/";
	}
	
	@RequestMapping(value="/signin.do", method=RequestMethod.POST)
	public String signInDo(Model model, HttpServletRequest request, HttpSession session, memberDTO memberDTO, RedirectAttributes redirectAttributes) throws Exception {
		
		String id = memberDTO.getId();
		String pw = memberDTO.getPw();
		
		if(id == "" || pw == "" || memberDTO == null)
			return "redirect:signin";
		
		
		memberDTO dto = new memberDTO();
		dto.setId(id); dto.setPw(pw);
		
		memberDTO vo = memberService.selectIdPw(dto);
		if(vo == null || vo.getId() == "" || vo.getPw() == "")
			return "redirect:signin";
		
		// 유저가 입력한 아이디, 비밀번호와 매칭되는 Member 컬럼이 존재하는 경우 그대로 반환하기 때문에 null값이나 empty값은 아이디 혹은 비밀번호가 틀렸다는 것과 같다.
		
		redirectAttributes.addAttribute("loginID", vo.getId());
		memberService.login(vo, session); // 로그인 처리
		return "redirect:/";
	}
	
	@RequestMapping(value="/signup.do", method=RequestMethod.POST)
	public String signUpDo(Model model, HttpServletRequest request, memberDTO memberDTO) throws Exception {
		
		memberService.insert(memberDTO);
		return "redirect:/";
	}
	
	@RequestMapping(value="/signUpValidate", method=RequestMethod.POST)
	@ResponseBody
	public boolean signUpValidate(HttpServletRequest request, HttpSession session, @RequestBody HashMap<String, Object> map) throws Exception {
		
		// if id is empty,
		if(map.get("id").equals(""))
			return true;
		
		memberDTO vo = new memberDTO(); vo.setId(map.get("id").toString()); // vo initial
		vo = memberService.selectId(vo); // find memberDTO with param(id)
		
		// if vo is null
		if(vo == null) 
			return true;
		
		// if vo.getId is empty,
		if(vo.getId().equals(""))
			return true;
		else
			return false;
	}
}
