package com.minh.board.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.minh.board.DTO.memberDTO;
import com.minh.board.service.memberService;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	memberService memberService;
	
	@RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
	public String home(Model model, HttpServletRequest request, HttpSession session, @RequestParam(value="loginID", defaultValue="")String loginID) throws Exception {
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true) {
			// 로그인 했을 때 세션 정보에 대한 member의 데이터를 구해온다.
			vo = memberService.selectSession(session);
		}
		
		if(vo != null) {
			model.addAttribute("isLogin", true);
			model.addAttribute("member", vo);
		}
		else {
			model.addAttribute("isLogin", false);
		}
		
		model.addAttribute("loginID", loginID);
		
		return "home";
	}
	
}
