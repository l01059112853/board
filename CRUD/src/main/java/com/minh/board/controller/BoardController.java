package com.minh.board.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardThumbnailDTO;
import com.minh.board.DTO.boardUploadFileDTO;
import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.service.boardService;
import com.minh.board.service.boardThumbnailService;
import com.minh.board.service.boardUploadFileService;
import com.minh.board.service.commentService;
import com.minh.board.service.memberService;
import com.minh.board.util.paging;
import com.minh.board.util.util;

@Controller
@PropertySource({"classpath:profiles/${spring.profiles.active}/application.properties"})
public class BoardController {
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	private static final int screenPage = 5; // list페이지에 출력될 페이지의 개수 (ex: 1|2|3|4|5 )
	private static final int screenBoard = 5; // list페이지에 한 화면에 출력될 게시글의 개수
	private static final int gridScreenBoard = 9; // gridview에서 list페이지에 출력될 게시글의 개수
	
	@Value("${upload.location}")
	String UPLOAD_PATH;
	
	@Value("${upload.uri_path}")
	String UPLOAD_URL;
	
	@Autowired
	boardService boardService;
	
	@Autowired
	memberService memberService;
	
	@Autowired
	commentService commentService;
	
	@Autowired
	boardUploadFileService boardUploadFileService;
	
	@Autowired
	boardThumbnailService boardThumbnailService;
	
	@RequestMapping(value="list", method=RequestMethod.GET)
	public String list(Model model, HttpServletRequest request, HttpSession session) throws Exception {
		
		String page = request.getParameter("page");
		if(page == null)
			page = "1";
		
		String search = request.getParameter("search");
		// search is need not checking. because search is nullable.
		
		memberDTO loginData = null;
		
		if(memberService.isLogin(session) == true) {
			loginData = memberService.selectSession(session);
			
			model.addAttribute("isLogin", true);
			model.addAttribute("member", loginData);
		}
		else {
			model.addAttribute("isLogin", false);
		}
		
		int totalBoard = boardService.selectCount(search);

		HashMap<String, Integer> map = paging.get(totalBoard, screenPage, screenBoard, Integer.parseInt(page));
		int head = (Integer.parseInt(page) - 1) * screenBoard;
		
		List<boardDTO> boards = boardService.select(search, head, screenBoard);
		List<Integer> boardCmtCount = new ArrayList<Integer>();
		
		for(boardDTO vo : boards) {
			boardCmtCount.add(commentService.selectRootCount(vo.getNo()));
			vo.setDate(util.timeConvert(vo.getDate(), "YYMMDD HHMM"));
			vo.setMaster(util.md5(vo.getMaster()));
		}
		
		model.addAttribute("board", boards);
		model.addAttribute("boardCmt", boardCmtCount);
		model.addAttribute("map", map);
		model.addAttribute("boardCount", totalBoard);
		
		if(search != null) {
			model.addAttribute("search", search);
		}
		
		return "list";
	}
	
	@RequestMapping(value="grid", method=RequestMethod.GET)
	public String gridList(Model model, HttpServletRequest request, HttpSession session) throws Exception {

		String search = request.getParameter("search");
		// search is need not checking. because search is nullable.
		
		memberDTO loginData = null;
		
		if(memberService.isLogin(session) == true) {
			loginData = memberService.selectSession(session);
			
			model.addAttribute("isLogin", true);
			model.addAttribute("member", loginData);
		}
		else {
			model.addAttribute("isLogin", false);
		}
		
		int totalBoard = boardService.selectCount(search);
		
		List<boardDTO> boards = boardService.select(search, 0, gridScreenBoard);
		List<Integer> boardCmtCount = new ArrayList<Integer>();
		List<String> boardThumbnail = new ArrayList<String>();
		List<Boolean> boardUploadFile = new ArrayList<Boolean>();
		
		for(boardDTO vo : boards) {
			// getting Count
			boardCmtCount.add(commentService.selectRootCount(vo.getNo()));
			vo.setDate(util.timeConvert(vo.getDate(), "YYMMDD HHMM"));
			vo.setMaster(util.md5(vo.getMaster()));
			
			// getting Thumbnail
			boardThumbnail.add(boardThumbnailService.selectNo(vo).getPath());
			
			if(!boardUploadFileService.selectNo(vo).getUploadPath().equals(""))
				boardUploadFile.add(true);
			else
				boardUploadFile.add(false);
		}
		
		model.addAttribute("board", boards);
		model.addAttribute("boardCmt", boardCmtCount);
		model.addAttribute("boardThumbnail", boardThumbnail);
		model.addAttribute("boardUploadFile", boardUploadFile);
		model.addAttribute("gridScreenCount", gridScreenBoard);
		
		if(search != null) {
			model.addAttribute("search", search);
		}
		
		return "grid";
	}
	
	@RequestMapping(value="seeMore", method=RequestMethod.GET)
    @ResponseBody
	public HashMap<String, Object> gridList(Model model, HttpServletRequest request,
    @RequestBody @RequestParam(value="limitStart", required=false, defaultValue="") String limitStart,
	@RequestBody @RequestParam(value="search", required=false) String search) throws Exception {

		HashMap<String, Object> map = new HashMap<String, Object>();
		// map for return
		
		String getSearch = request.getParameter("search");
        // search is need not checking. because search is nullable.

        int limit = 0;

        if(limitStart != null && !limitStart.equals(""))
            limit = Integer.parseInt(limitStart);
        
        logger.info("limit: " + limit);
		
		List<boardDTO> boards = boardService.select(getSearch, limit, gridScreenBoard);
		if(boards.size() <= 0) {
			map.put("status", false);
			return map;
		}
		
		List<Integer> boardCmtCount = new ArrayList<Integer>();
		List<String> boardThumbnail = new ArrayList<String>();
		List<Boolean> boardUploadFile = new ArrayList<Boolean>();
		
		for(boardDTO vo : boards) {
			// getting Count
			boardCmtCount.add(commentService.selectRootCount(vo.getNo()));
			vo.setDate(util.timeConvert(vo.getDate(), "YYMMDD HHMM"));
			vo.setMaster(util.md5(vo.getMaster()));
			
			// getting Thumbnail
			boardThumbnail.add(boardThumbnailService.selectNo(vo).getPath());
			
			if(!boardUploadFileService.selectNo(vo).getUploadPath().equals(""))
				boardUploadFile.add(true);
			else
				boardUploadFile.add(false);
		}
		
		map.put("board", boards);
		map.put("boardCmt", boardCmtCount);
		map.put("boardThumbnail", boardThumbnail);
		map.put("boardUploadFile", boardUploadFile);
		map.put("gridScreenCount", limit + gridScreenBoard);
		map.put("status", true);
		
		if(search != null) {
			model.addAttribute("search", search);
		}
		
		return map;
	}
	
	@RequestMapping(value="search", method=RequestMethod.GET)
	@ResponseBody
	public boolean search(HttpServletRequest request, @RequestBody @RequestParam(value="search", required=false, defaultValue="") String search) throws Exception {
		
		logger.info("search: " + search);
		
		if(search.equals(""))
			search = null;
		
		List<boardDTO> lists = boardService.select(search, 0, 5);
		
		if(lists.size() <= 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	@RequestMapping(value="detail", method=RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpSession session) throws Exception {
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true) {
			// 로그인 했을 때 세션 정보에 대한 member의 데이터를 구해온다.
			vo = memberService.selectSession(session);
		}
		
		if(vo != null) {
			model.addAttribute("isLogin", true);
			model.addAttribute("member", vo);
		}
		else {
			model.addAttribute("isLogin", false);
		}
		
		String no = request.getParameter("no");
		
		if(no == null) {
			return "redirect:list";
		}
		
		boardDTO boardDTO = boardService.selectNo(Integer.parseInt(no));
		boardDTO.setDate(util.timeConvert(boardDTO.getDate(), "YYMMDD HHMM"));
		logger.info("boardDTO.getMaster() in detail Page : " + boardDTO.getMaster());
		boardDTO.setMaster(util.md5(boardDTO.getMaster()));
		logger.info("boardDTO.getMaster() in detail Page AFTER md5 : " + boardDTO.getMaster());
		
		model.addAttribute("board", boardDTO);
		
		List<commentDTO> commentList = commentService.selectRoot(Integer.parseInt(no));
		int commentCount = commentService.selectRootCount(Integer.parseInt(no));
		boolean myCommentIsExist = false;
		
		if(vo != null) {
			for(commentDTO cmtDTO : commentList) {
				if(cmtDTO.getWriter().equals(vo.getId())) {
					myCommentIsExist = true;
					break;
				}
			}
		}
		
		logger.info("myCommentIsExist: " + myCommentIsExist);
		
		model.addAttribute("comment", commentList);
		model.addAttribute("commentCount", commentCount);
		model.addAttribute("myCommentIsExist", myCommentIsExist);
		
		// 현 게시글에 업로드 된 파일을 가지고 오기 위한 서비스
		boardUploadFileDTO boardUploadFileDTO = boardUploadFileService.selectNo(boardDTO);
		if(boardUploadFileDTO != null) {
			if(!boardUploadFileDTO.getOriginName().equals("") && !boardUploadFileDTO.getUploadPath().equals("")) {
				model.addAttribute("uploadFileExists", true);
				model.addAttribute("boardUploadFile", boardUploadFileDTO);
			}
			else {
				model.addAttribute("uploadFileExists", false);	
			}
		}
		else {
			model.addAttribute("uploadFileExists", false);
		}
		
		// 현 게시글에 업로드 된 썸네일을 가지고 오기 위한 서비스
		boardThumbnailDTO boardThumbnailDTO = boardThumbnailService.selectNo(boardDTO);
		if(boardThumbnailDTO != null) {
			if(!boardThumbnailDTO.getPath().equals("")) {
				model.addAttribute("thumbnailExists", true);
				model.addAttribute("boardThumbnail", boardThumbnailDTO);
			}
			else {
				model.addAttribute("thumbnailExists", false);
			}
		}
		else {
			model.addAttribute("thumbnailExists", false);
		}
		
		return "detail";
	}
	
	@RequestMapping(value="write", method=RequestMethod.GET)
	public String write(Model model, HttpServletRequest request, HttpSession session) throws Exception {
		
		memberDTO vo = null;
		
		// 로그인하지 않았을 경우
		if(memberService.isLogin(session) == false) {
			return "redirect:list";
		}
		
		vo = memberService.selectSession(session);
		
		if(vo != null) {
			model.addAttribute("member", vo);
			model.addAttribute("isLogin", true);
		}
		else {
			// 세션에 알맞는 member 데이터가 존재하지 않을경우
			return "redirect:list";
		}
		
		return "write";
	}
	
	@RequestMapping(value="update", method= {RequestMethod.GET, RequestMethod.POST})
	public String update(Model model, HttpServletRequest request, HttpSession session, boardDTO boardDTO) throws Exception {
		
		// 게시글에 대한 no를 구해왔을 때 정상적인 no를 구해오지 못한 경우(0 이하의 값) 
		int no = boardDTO.getNo();
		if(no <= 0) {
			logger.info("no <= 0");
			return "redirect:list";
		}
		
		// 게시글에 대한 비밀번호를 구해온 경우
		String boardMaster = request.getParameter("boardMaster");
		if(boardMaster == null) {
			logger.info("boardMaster is null");
			return "redirect:list";
		}
		
		// 게시글에서 입력한 비밀번호를 md5 암호화를 적용시켰을 때 해당 게시글의 비밀번호(이 비밀번호도 md5 암호화가 되어있음)와 동일하지 않은 경우
		String inputPw = boardDTO.getMaster();
		logger.info("inputPw : " + inputPw);
		inputPw = util.md5(inputPw);
		
		if(!boardMaster.equals(inputPw)) {
			logger.info("boardMaster is not equals inputPw.");
			logger.info("boardMaster: " + boardMaster + ", inputPw: " + inputPw);
			return "redirect:detail?no=" + no;
		}
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true) {
			// 로그인 했을 때 세션 정보에 대한 member의 데이터를 구해온다.
			vo = memberService.selectSession(session);
		} else {
			// 로그인 되어있지 않은 경우
			logger.info("not login.");
			return "redirect:detail?no=" + no;
		}
		
		if(vo == null) {
			// 로그인한 사용자의 데이터를 불러올 수 없는 경우
			logger.info("vo is null");
			return "redirect:detail?no=" + no;
		}
		
		// 게시글 작성자와 현재 로그인한 사용자의 아이디와 일치하지 않는 경우
		if(!vo.getId().equals(boardDTO.getWriter())) {
			logger.info("id of vo is not equals writer of boardDTO");
			logger.info("vo.getId() : " + vo.getId() + ", boardDTO.getWriter() : " + boardDTO.getWriter());
			return "redirect:detail?no=" + no;
		}
		
		// 현재 게시글 번호에 해당하는 board 데이터를 조회한다.
		boardDTO board = boardService.selectNo(no);
		
		// model setting
		model.addAttribute("board", board);
		model.addAttribute("isLogin", true);
		model.addAttribute("member", vo);
		
		// 현 게시글에 업로드 된 파일을 가지고 오기 위한 서비스
		boardUploadFileDTO boardUploadFileDTO = boardUploadFileService.selectNo(boardDTO);
		if(boardUploadFileDTO != null) {
			if(!boardUploadFileDTO.getOriginName().equals("") && !boardUploadFileDTO.getUploadPath().equals("")) {
				model.addAttribute("uploadFileExists", true);
				model.addAttribute("boardUploadFile", boardUploadFileDTO);
			}
			else {
				model.addAttribute("uploadFileExists", false);	
			}
		}
		else {
			model.addAttribute("uploadFileExists", false);
		}
		
		// 현 게시글에 업로드 된 썸네일을 가지고 오기 위한 서비스
		boardThumbnailDTO boardThumbnailDTO = boardThumbnailService.selectNo(boardDTO);
		if(boardThumbnailDTO != null) {
			if(!boardThumbnailDTO.getPath().equals("")) {
				model.addAttribute("thumbnailExists", true);
				model.addAttribute("boardThumbnail", boardThumbnailDTO);
			}
			else {
				model.addAttribute("thumbnailExists", false);
			}
		}
		else {
			model.addAttribute("thumbnailExists", false);
		}
		
		return "update";
	}
	
	@RequestMapping(value="delete", method=RequestMethod.GET)
	public String delete(HttpSession session, HttpServletRequest request) throws Exception{
		
		String no, writer;
		
		no = request.getParameter("no");
		writer = request.getParameter("writer");
		
		if(no == null || writer == null)
			return "redirect:list";
		
		boardDTO boardDTO = boardService.selectNo(Integer.parseInt(no));
		
		if(boardDTO == null)
			return "redirect:list";
		
		if(boardDTO.getNo() <= 0)
			return "redirect:list";
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true)
			vo = memberService.selectSession(session);
		else
			return "redirect:list";
		
		if(vo == null)
			return "redirect:list";
		
		if(!vo.getId().equals(boardDTO.getWriter()))
			return "redirect:list";
		
		String host = request.getRequestURL().toString().replace(request.getRequestURI(), "");
		
		// 게시글 파일 존재 시
		boardUploadFileDTO boardUploadFileDTO = boardUploadFileService.selectNo(boardDTO);
		if(boardUploadFileDTO != null) {
			if(!boardUploadFileDTO.getOriginName().equals("") || !boardUploadFileDTO.getUploadPath().equals("")) {
				File deleteFile = new File(host + "/Upload", UPLOAD_PATH);
				
				if(deleteFile.delete() == true)
					logger.info(boardDTO.toString() + " UploadFile Delete.");
			}
		}
		
		boardThumbnailDTO boardThumbnailDTO = boardThumbnailService.selectNo(boardDTO);
		if(boardThumbnailDTO != null) {
			if(!boardThumbnailDTO.getPath().equals("")) {
				File deleteFile = new File(host + "/Upload", UPLOAD_PATH);
				
				if(deleteFile.delete() == true)
					logger.info(boardDTO.toString() + " Thumbnail Delete.");
			}
		}
		
		boardService.delete(boardDTO);
		return "redirect:list";
	}
	
	@RequestMapping(value="write.do", method=RequestMethod.POST)
	public String writeDo(Model model, HttpSession session, HttpServletRequest request, boardDTO boardDTO,
	@RequestParam(value="file", required=false) MultipartFile multipartFile, @RequestParam(value="thumbnail", required=false) MultipartFile multipartImage) throws IOException, Exception {
		
		if(boardDTO == null)
			return "redirect:list";
		
		if(boardDTO.getWriter() == "" || boardDTO.getSubject() == "" || boardDTO.getBody() == "" || boardDTO.getMaster() == "") {
			return "redirect:list";
		}
		
		String originalFile = "", originalImage = "", fileExtension = "", imageExtension = "", fileUuid = "", imageUuid = "";
		byte[] fileData, imageData;
		FileOutputStream fileOutputStream = null, imageOutputStream = null;
/*
 * 		MultipartHttpServletRequest를 이용한 파일 업로드
 * 		
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> multipartFileList = new ArrayList<>();
		
		multipartFileList.add(multipartHttpServletRequest.getFile("file"));
		multipartFileList.add(multipartHttpServletRequest.getFile("thumbnail"));
		
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		while(iterator.hasNext()) {
			String uploadPath = iterator.next();
			logger.info("uploadPath: " + uploadPath);
			
			MultipartFile mFile = multipartHttpServletRequest.getFile(uploadPath);
			logger.info("originalFileName: " + mFile.getOriginalFilename());
		}
*/
		
		boardService.insert(boardDTO); // inserting data in board table.
		int no = boardDTO.getNo(); // inquire data that inserting recently. and using for redirect.
		
		// Triggers occur after uploading. So, Call the update service immediately.
		
		File directory = new File(UPLOAD_PATH);
		if(!directory.exists())
			directory.mkdirs();
		
		String host = request.getRequestURL().toString().replace(request.getRequestURI(), "");
		
		if(multipartFile.isEmpty() == false) {
			originalFile = multipartFile.getOriginalFilename();
			fileExtension = originalFile.substring(originalFile.lastIndexOf("."));
			fileUuid = UUID.randomUUID().toString().replaceAll("-",  "");
			fileData = multipartFile.getBytes();
			
			String filePath = host + UPLOAD_URL + "/" + fileUuid + fileExtension;
			
			fileOutputStream = new FileOutputStream(UPLOAD_PATH + "/" + fileUuid + fileExtension);
			fileOutputStream.write(fileData);
			
			if(fileOutputStream != null) {
				fileOutputStream.close();
			}
			
			boardUploadFileDTO boardUploadFileDTO = new boardUploadFileDTO();
			
			boardUploadFileDTO.setNo(no);
			boardUploadFileDTO.setOriginName(originalFile);
			boardUploadFileDTO.setUploadPath(filePath);
			
			boardUploadFileService.update(boardUploadFileDTO);
		}
		
		if(multipartImage.isEmpty() == false) {
			originalImage = multipartImage.getOriginalFilename();
			imageExtension = originalImage.substring(originalImage.lastIndexOf("."));
			imageUuid = UUID.randomUUID().toString().replaceAll("-", "");
			imageData = multipartImage.getBytes();
			
			String imagePath = host + UPLOAD_URL + "/" + imageUuid + imageExtension;
			
			imageOutputStream = new FileOutputStream(UPLOAD_PATH + "/" + imageUuid + imageExtension);
			imageOutputStream.write(imageData);
			
			if(imageOutputStream != null) {
				imageOutputStream.close();
			}
			
			boardThumbnailDTO boardThumbnailDTO = new boardThumbnailDTO();
			
			boardThumbnailDTO.setNo(no);
			boardThumbnailDTO.setPath(imagePath);
			
			boardThumbnailService.update(boardThumbnailDTO);
		}
		
		return "redirect:detail?no=" + no;
	}
	
	@RequestMapping(value="update.do", method=RequestMethod.POST)
	public String updateDo(Model model, HttpServletRequest request, HttpSession session, boardDTO boardDTO,
	@RequestParam(value="file", required=false) MultipartFile multipartFile, @RequestParam(value="thumbnail", required=false) MultipartFile multipartImage) throws IOException, Exception {
		
		if(boardDTO == null)
			return "redirect:list";
		
		int no = boardDTO.getNo();
		if(no <= 0)
			return "redirect:list";
		
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true)
			vo = memberService.selectSession(session);
		else
			return "redirect:detail?no=" + no;
		
		if(vo == null)
			return "redirect:detail?no=" + no;
		
		if(!vo.getId().equals(boardDTO.getWriter()))
			return "redirect:detail?no=" + no;
		
		boardService.update(boardDTO);
		boardUploadFileDTO boardUploadFileVO = boardUploadFileService.selectNo(boardDTO);
		boardThumbnailDTO boardThumbnailVO = boardThumbnailService.selectNo(boardDTO);
		
		String originalFile = "", originalImage = "", fileExtension = "", imageExtension = "", fileUuid = "", imageUuid = "";
		byte[] fileData, imageData;
		FileOutputStream fileOutputStream = null, imageOutputStream = null;
		
		File directory = new File(UPLOAD_PATH);
		if(!directory.exists())
			directory.mkdirs();
		
		String host = request.getRequestURL().toString().replace(request.getRequestURI(), "");
		
		if(multipartFile.isEmpty() == false && boardUploadFileVO != null) {
			originalFile = multipartFile.getOriginalFilename();
			fileExtension = originalFile.substring(originalFile.lastIndexOf("."));
			fileUuid = UUID.randomUUID().toString().replaceAll("-",  "");
			fileData = multipartFile.getBytes();
			
			// 새로운 파일을 올렸을 때
			if(!boardUploadFileVO.getOriginName().equals(originalFile)){
				String deletePath = boardUploadFileVO.getUploadPath();					// 기존 파일 삭제를 위한 경로 구해오기
				deletePath = deletePath.replace(host + "/Upload", UPLOAD_PATH);			// 실제 파일이 존재하는 절대경로 접근을 위한 작업
				logger.info(deletePath);
				
				File deleteFile = new File(deletePath);
				logger.info(boardUploadFileVO.toString());
				if(deleteFile.exists()) {
					if(deleteFile.delete()) {
						logger.info("file delete Success");
					}
					else {
						logger.info("file delete failed");
					}
				}
				else {
					logger.info("file is not exists");
				}
				
				String filePath = host + UPLOAD_URL + "/" + fileUuid + fileExtension;
				
				fileOutputStream = new FileOutputStream(UPLOAD_PATH + "/" + fileUuid + fileExtension);
				fileOutputStream.write(fileData);
			
				if(fileOutputStream != null) {
					fileOutputStream.close();
				}
				
				boardUploadFileDTO boardUploadFileDTO = new boardUploadFileDTO();
				
				boardUploadFileDTO.setNo(no);
				boardUploadFileDTO.setOriginName(originalFile);
				boardUploadFileDTO.setUploadPath(filePath);
				
				boardUploadFileService.update(boardUploadFileDTO); // File Update
			}
		}
		
		if(multipartImage.isEmpty() == false && boardThumbnailVO != null) {
			originalImage = multipartImage.getOriginalFilename();
			imageExtension = originalImage.substring(originalImage.lastIndexOf("."));
			imageUuid = UUID.randomUUID().toString().replaceAll("-", "");
			imageData = multipartImage.getBytes();
			
			// 새로운 썸네일을 올렸을 때
			if(!boardThumbnailVO.getPath().equals(originalImage)) {
				String deletePath = boardThumbnailVO.getPath();
				deletePath = deletePath.replace(host + "/Upload", UPLOAD_PATH);
				logger.info(deletePath);
				
				File deleteFile = new File(deletePath);
				logger.info(boardThumbnailVO.toString());
				if(deleteFile.exists()) {
					if(deleteFile.delete()) {
						logger.info("thumbnail delete Success");
					}
					else {
						logger.info("thumbnail delete failed");
					}
				}
				else {
					logger.info("thumbnail File is not exists");
				}
				
				String imagePath = host + UPLOAD_URL + "/" + imageUuid + imageExtension;
				
				imageOutputStream = new FileOutputStream(UPLOAD_PATH + "/" + imageUuid + imageExtension);
				imageOutputStream.write(imageData);
			
				if(imageOutputStream != null) {
					imageOutputStream.close();
				}
				
				boardThumbnailDTO boardThumbnailDTO = new boardThumbnailDTO();
				
				boardThumbnailDTO.setNo(no);
				boardThumbnailDTO.setPath(imagePath);
				
				boardThumbnailService.update(boardThumbnailDTO); // Thumbnail Update
			}
		}
		
		model.addAttribute("isLogin", true);
		model.addAttribute("member", vo);
		
		return "redirect:detail?no=" + no;
	}
}
