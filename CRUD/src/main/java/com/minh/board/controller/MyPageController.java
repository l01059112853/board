package com.minh.board.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.minh.board.DTO.boardDTO;
import com.minh.board.DTO.boardThumbnailDTO;
import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.DTO.memberProfileDTO;
import com.minh.board.service.boardService;
import com.minh.board.service.boardThumbnailService;
import com.minh.board.service.commentService;
import com.minh.board.service.memberProfileService;
import com.minh.board.service.memberService;
import com.minh.board.util.util;

@Controller
public class MyPageController {

	private static final Logger logger = LoggerFactory.getLogger(MyPageController.class);
	
	@Autowired
	memberService memberService;
	
	@Autowired
	memberProfileService memberProfileService;
	
	@Autowired
	boardService boardService;
	
	@Autowired
	boardThumbnailService boardThumbnailService;
	
	@Autowired
	commentService commentService;
	
	@RequestMapping(value = "mypage/{userID}", method=RequestMethod.GET)
	public String mypage(Model model, HttpServletRequest request, HttpSession session, @PathVariable("userID") String userID) throws Exception {
		memberDTO vo = null;
		
		if(memberService.isLogin(session) == true) {
			vo = memberService.selectSession(session);
		}
		
		if(vo != null) {
			model.addAttribute("isLogin", true);
			model.addAttribute("member", vo);
		}
		else {
			model.addAttribute("isLogin", false);
		}
		
		memberDTO dto = new memberDTO();
		dto.setId(userID);
		
		if(dto.getId().equals("")) {
			logger.info("dto.getId() is \"\"");
			return "redirect:/";
		}
		
		if(memberService.selectId(dto) == null) {
			logger.info("memberService.selectId(dto) is null");
			return "redirect:/";
		}
		else {
			memberProfileDTO memberProfileDTO = memberProfileService.selectId(dto);
			
			if(memberProfileDTO != null) {
				if(memberProfileDTO.getPath().equals("")) {
					memberProfileDTO.setPath("resources/IMG/avatar.jpg"); // default Image
				}
				
				logger.info("memberProfileDTO.getPath() : " + memberProfileDTO.getPath());
				
				model.addAttribute("path", memberProfileDTO.getPath());
			}
			
			/* 마이페이지 주인(?)의 최근 작성한 게시글 */
			List<boardDTO> boardList = boardService.selectMyPage(dto.getId());
			List<boardThumbnailDTO> boardThumbnailList = new ArrayList<>();
			
			for(boardDTO boardDTO : boardList) {
				boardDTO.setDate(util.timeConvert(boardDTO.getDate(), "YYMMDD HHMM"));
				boardThumbnailList.add(boardThumbnailService.selectNo(boardDTO));
			}
			
			if(boardList != null && boardThumbnailList != null) {
				model.addAttribute("boardList", boardList);
				model.addAttribute("boardThumbnailList", boardThumbnailList);
				model.addAttribute("boardStatus", true);
			}
			else {
				model.addAttribute("boardStatus", false);
			}
			
			/* 마이페이지 주인(?)의 최근 댓글을 단 게시글 */
			commentDTO commentDTO = new commentDTO();
			commentDTO.setWriter(dto.getId());
			
			List<commentDTO> commentListByWriter = commentService.selectWriter(commentDTO); // 댓글을 단 게시글 List (dto의 root를 이용하여 게시글의 정보를 조회한다.)
			List<boardDTO> boardListByComment = new ArrayList<boardDTO>(); // 댓글을 단 게시글을 저장할 List (dto를 이용하여 썸네일을 조회한다.)
			List<boardThumbnailDTO> boardThumbnailListByComment = new ArrayList<boardThumbnailDTO>();
			
			if(commentListByWriter.size() > 0) {
				
				for(commentDTO cmtVo : commentListByWriter) {
					if(cmtVo.getRoot() > 0) {
						
						if(cmtVo.getBody().length() > 10)
							cmtVo.setBody(cmtVo.getBody().substring(0, 10) + "…"); // 댓글의 길이가 10자 이상인 경우 댓글 내용 일부만 보여줄 수 있도록 하기 위한 작업
						
						boardDTO brdVo = boardService.selectNo(cmtVo.getRoot()); // 댓글을 단 게시글에 해당하는 게시글 번호를 구해온 뒤
						boardListByComment.add(brdVo); // List에 추가한다.
					}
					else {
						boardListByComment.add(null); // index 맞추기
					}
				}

				for(boardDTO brdVo : boardListByComment) {
					if(brdVo.getNo() > 0) {
						boardThumbnailDTO brdTnVo = boardThumbnailService.selectNo(brdVo); // 게시글 DTO의 번호를 토대로 썸네일 path를 가진 dto를 조회한다.
						boardThumbnailListByComment.add(brdTnVo); // 조회한 dto를 list에 추가한다.
					}
					else {
						boardThumbnailListByComment.add(null); // index 맞추기
					}
				}
				
				model.addAttribute("commentListByWriter", commentListByWriter);
				model.addAttribute("boardListByComment", boardListByComment);
				model.addAttribute("boardThumbnailListByComment", boardThumbnailListByComment);
				model.addAttribute("commentStatus", true);
			}
			else {
				model.addAttribute("commentStatus", false);
			}
			
			model.addAttribute("userID", userID);
			return "mypage";
		}
	}
}
