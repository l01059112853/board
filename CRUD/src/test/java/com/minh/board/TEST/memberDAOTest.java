package com.minh.board.TEST;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.minh.board.DAO.memberDAO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.config.RootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={RootConfig.class})
public class memberDAOTest {
	
	private static final Logger logger = LoggerFactory.getLogger(memberDAOTest.class);
	
	@Autowired
	memberDAO memberDAO;
	
	@Test
	public void selectTest() throws Exception{
		
/*		String id = "test02";
		String pw = "5678";
		memberDTO memberDTO = new memberDTO();
		
		memberDTO.setId(id);
		memberDTO.setPw(pw);
		memberDTO.setName("catalina");
		
		memberDAO.insert(memberDTO);
		logger.info("member Data Insert");
		
		memberDTO memberVO = memberDAO.selectIdPw(memberDTO);
		logger.info("selectIdPw() : " + memberVO.toString());*/
		
		memberDTO vo = new memberDTO();
		vo.setId("1234");
		
		boolean chk = memberDAO.selectId(vo) == null;
		
		logger.info("1234 exist: " + chk);
		
		vo.setId("test01");
		chk = memberDAO.selectId(vo) == null;
		logger.info("test01 exist: " + chk);
	}
}
