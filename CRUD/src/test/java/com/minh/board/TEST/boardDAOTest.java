package com.minh.board.TEST;

import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.minh.board.DAO.boardDAO;
import com.minh.board.DTO.boardDTO;
import com.minh.board.config.RootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={RootConfig.class})
public class boardDAOTest {
	
	private static final Logger logger = LoggerFactory.getLogger(boardDAOTest.class);
	
	@Autowired
	boardDAO boardDAO;
	
	@Test
	public void test() throws Exception{
/*		List<boardDTO> list = boardDAO.select(null, 0, 5);
		
		for(boardDTO vo : list) {
			logger.info(vo.toString());
		}
		
		list = boardDAO.select("한 무덤", 0, 5);
		for(boardDTO vo : list) {
			logger.info(vo.toString());
		}
*/
		int count = boardDAO.selectCount(null);
		logger.info("parameter is null. count : " + count);
		
		count = boardDAO.selectCount("한 무덤");
		logger.info("parameter is exist. count : " + count);
	}
}
