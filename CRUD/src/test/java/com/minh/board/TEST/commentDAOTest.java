package com.minh.board.TEST;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.minh.board.DAO.commentDAO;
import com.minh.board.DTO.commentDTO;
import com.minh.board.DTO.memberDTO;
import com.minh.board.config.RootConfig;
import com.minh.board.util.util;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={RootConfig.class})
public class commentDAOTest {
	
	private static final Logger logger = LoggerFactory.getLogger(commentDAOTest.class);
	
	@Autowired
	commentDAO commentDAO;
	
	@Test
	public void commentTest() throws Exception{
		commentDTO commentDTO = new commentDTO();
		commentDTO.setWriter("test01");
		
		List<commentDTO> commentList = commentDAO.selectWriter(commentDTO);
		for(commentDTO vo : commentList) {
			logger.info(vo.toString());
		}
		
	}
/*	public void insertingTest() throws Exception{
		commentDTO commentDTO = new commentDTO();
		String body = "<h4>aa</h4>happy~! :D";
		body = util.htmlspecialchars(body);
		
		commentDTO.setWriter("testUser01");
		commentDTO.setBody(body);
		
		commentDTO.setNo(18);
		commentDAO.insert(commentDTO);		
		logger.info("commentDAO Inserting.");
		
		commentDTO vo = commentDAO.selectNo(1);
		logger.info("inserting test: " + vo.toString());
	}
*/
}
